#!/usr/bin/fish
#Porting to bash considered an act of masochism
echo (nproc)\n(math floor (math (free -g | grep -oP '\d+' | head -n 1)/2.2)) | sort -rn | tail -1
