#!/bin/sh

mkdir build_windows
cd build_windows

rm CMakeCache.txt
cmake .. -DWINDOWS=on -DCMAKE_TOOLCHAIN_FILE=../module2d/mingw.toolchain -DRELEASE=on
make -j$(../.CORES.sh)
