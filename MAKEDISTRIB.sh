#!/usr/bin/fish

rm -rf distrib
mkdir distrib
cp -rf spacegame module2d distrib/
cp *.sh LICENSE  CMakeLists.txt .CORES.sh distrib/
distrib/
rm -rf (find . -name ".git")
./BUILD.sh
./WINCOMPL.sh
for i in build_release build_windows
	cd $i
	rm -rf (find . -not -name "Spacegame")
	../
end
