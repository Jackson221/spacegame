#!/bin/sh

#Fetch dependencies "universal install script" style and then build

pkexec bash -c '''
apt install g++ git cmake libglm-dev libvulkan-dev libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev fish glslang-tools
'''

#Other disto users are more than welcome to add their own package manger's packages, there's no need to fight, this script doesn't set -e for a reason :)

./BUILD.SH
