/*!
 * @file
 * @author Jackson McNeill
 * Game-specific classes (not part of the core engine)
 * Also holds other things specific to this game.
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <future>
#include <memory>


#include "render/object.hpp"
#include "render/state.hpp"
#include "simulation/physics.hpp"
#include "synchro/enumeration.hpp"
//#include "level_editor.hpp" //TODO MEGA

#include "input/computer.hpp"
#include "input/axis.hpp"
#include "input/remappable.hpp"

#include "simulation/netplay.hpp"

#include "worldinput/mouse.hpp"

#include "engine.hpp"



//LIMITS:


#define MAX_SUBSYSTEMS 40

enum TYPES
{
	TYPE_REGULAR, //starts at 0
	TYPE_PLAYER,
	TYPE_ENEMY,
	TYPE_DESTROYSUB, //bullets, etc.
	TYPE_SHIPWALL,
	TYPE_RESERVED,
	TYPE_COUNT // ammount of types that exist - do not use as a type
};

enum GROUPS
{
	GROUP_REGULAROBJECT,	
	GROUP_INTELIGENT, 
	GROUP_COUNT
};

extern bool typeInGroup[TYPE_COUNT][GROUP_COUNT];

//config

#define STD_BULLET_MASS 2


enum class player_control_axes
{
	forward,
	perpendicular,
	angular,
	SIZE
};
enum class player_control_buttons
{
	shoot,
	dampen,
	SIZE
};


//Each local player will have an instance of this
struct player_controls
{
	input::remappable_axes_t<player_control_axes> axes;
	input::remappable_buttons_t<player_control_buttons> buttons;

	inline player_controls(input::interfaces & input_if) : 
		axes(input_if),
		buttons(input_if)
	{
	}
};

class spacegame_ecs_data;
class spacegame_ecs
{
	std::shared_ptr<spacegame_ecs_data> data;
	public:
		spacegame_ecs(sim::physen& physics, render::render_engine& rengine, engine_state& estate);
		~spacegame_ecs();

		size_t make_wall(mm::vec2r position);
};

/*

//TODO put serialization trackers here
struct classes_global_info;

extern std::promise<size_t> player_center_render_object_id_promise;

void InitClasses(render::state* rstate, player_controls& controls);

void ShutdownClasses();

class Subsystem
{
	public:
		Subsystem(render::state* rstate, std::string sprite, mm::vec2 size, mm::rect animation, mm::vec2r relativePosition, int collisionRectangle, std::string name, mm::vec2* parrentGrid);
		void CheckCollision(int checkCollisionRectangle);
		void Update(mm::vec2r parrentPosition);
		bool GetIsOnline() const;
		int GetLevel() const;
		void SetLevel(int level);

		double health; 
		const std::string name;

		using enumeration = synchro::enumeration<&Subsystem::health, &Subsystem::name>;
		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		render::object render_object;
	private:

		std::string spriteName;
		mm::vec2r const relativePosition;
		int const collisionRectangle;
		int level; //posibilites...
		mm::vec2* parrentGrid;
};

namespace action
{
	enum class action
	{
		move_forward,
		move_backward,
		move_left,
		move_right,
		rotate_left,
		rotate_right,
		try_shoot,
		DEBUG_stop
	};
	struct event_data_t
	{
		bool active;
		action the_action;
	};
	using net_data_t = netplay::networkable_event_data_t<event_data_t>;
	using event_configuration_types = netplay::event_configuration_types_plus_network_interface<event::event_configuration_types<net_data_t>>;
	using manager_t = event_configuration_types::manager_t;
	extern manager_t manager;
	using interface_t = event::endpoint_interface<event_configuration_types>;
	extern thread_local interface_t interface;
}
class Player;
namespace new_player
{
	struct event_data_t
	{
		Player* pointer_to_player;
		netplay::client_id_t controlling_player_id;
	};
	using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
	using manager_t = event_configuration_types::manager_t;
	extern manager_t manager;
	using interface_t = event::endpoint_interface<event_configuration_types>;
	extern thread_local interface_t interface;
}

void init_game();


#define CLASSES_SNAPSHOT_CONSTRUCTOR(class_name)\
	inline class_name ( synchro::Snapshot FUTURE_AUTO snapshot) :\
		rstate(snapshot.global_info.rstate)\
	{\
		snapshot.apply(this);\
	}

#define CLASSES_SYNC_POS_AND_VELOCITY(class_name)\
		using enumeration = synchro::enumeration\
		<\
			& class_name ::position,\
			& class_name ::velocity\
		>;\
		CLASSES_SNAPSHOT_CONSTRUCTOR( class_name );\
		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		


class Player : public sim::object, public level_editor::level_editor_interface
{
	netplay::client_id_t const my_controlling_player;
	input::computer::scan_code::interface_t scan_code_interface = input::computer::scan_code::interface_t(input::computer::scan_code::manager);
	worldinput::mouse::mouse_button::interface_t mouse_button_interface = worldinput::mouse::mouse_button::interface_t(worldinput::mouse::mouse_button::manager);

	input::generic_axis::interface_t forward_interface;
	input::generic_axis::interface_t perpendicular_interface;
	input::generic_axis::interface_t angular_interface;

	input::generic_button::interface_t shoot_interface;
	input::generic_button::interface_t dampen_interface;
	public:
		Player(render::state* rstate, netplay::client_id_t controlling_player_id, player_controls& controls);
		bool C_Update() override;
		bool C_Collision(sim::object* victim, int collisionRectangleID) override;
		bool GetIsOnline();
		std::vector<Subsystem> iterableSubsystems;

		static sim::object* spawnFromLevelEditor(render::state* rstate, mm::vec2r position);
		void setPosition(mm::vec2r position) override;
		const static std::string name;
		constexpr const static level_editor::level_editor_object_properties properties = level_editor::level_editor_object_properties{0,false};

		using enumeration = synchro::enumeration
		<
			//&Player::position,
			//&Player::velocity,
			&Player::my_controlling_player, 
			&Player::iterableSubsystems
		>;
		Player(synchro::Snapshot FUTURE_AUTO snapshot);
		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
	private:
		bool keysHeld[323] = {false}; 
		render::state* rstate;
		unsigned long long nextTickCanShoot;
		bool dampeners = false;

		event::listener<input::computer::scan_code::interface_t> scancode_listener;
		event::listener<worldinput::mouse::mouse_button::interface_t> mouse_button_listener;

		//event::listener<input::generic_button::interface_t> shoot_listener;

		input::generic_axis_value forward_axis = {forward_interface};
		input::generic_axis_value perpendicular_axis = {perpendicular_interface};
		input::generic_axis_value angular_axis = {angular_interface};

		input::generic_button_value shoot_button = {shoot_interface};
		input::generic_button_value dampen_button = {dampen_interface};
		

		mm::vec2 target_position = {0,0};
};

class Enemie : public sim::object, public level_editor::level_editor_interface
{
	public:
		Enemie(render::state* rstate, mm::vec2r position, int level);
		~Enemie();

		bool C_Update() override;
		bool C_Collision(sim::object* victim, int collisionRectangleID) override;

		static sim::object* spawnFromLevelEditor(render::state* rstate, mm::vec2r position);

		const static std::string name;
		constexpr const static level_editor::level_editor_object_properties properties = level_editor::level_editor_object_properties{0,false};

		CLASSES_SYNC_POS_AND_VELOCITY(Enemie);

	private:
		render::object render_object;
		render::state* rstate;
		int level;
		mm::vec2 lastFoundPlayer;
		int last_shot_tick;

		std::optional<sim::object*> currently_targetted_player;

		//GE_TrackMeAsSerializable tracker;

};

class BulletType : public sim::object, public level_editor::level_editor_interface
{
	public:
		BulletType(mm::vec2r position, mm::vec2r velocity,double mass);
		virtual ~BulletType() {}
		int level;
		bool C_Collision(sim::object* victim, int collisionRectangleID) override;

		virtual bool C_Update() override = 0;
};

class StdBullet : public BulletType //Hah
{
	public: 
		StdBullet(render::state* rstate, mm::vec2r position, std::string spriteName);

		bool C_Update() override;

		CLASSES_SYNC_POS_AND_VELOCITY(StdBullet);

	private:
		render::object render_object;
		render::state* rstate; //TODO
		
		std::string spriteName;
};


class Wall : public sim::object, public level_editor::level_editor_interface
{
	public:
		Wall(render::state* rstate, mm::vec2r position, mm::rectr shape, double mass);
		bool C_Collision(sim::object* victim, int collisionRectangleID) override;
		bool C_Update();

		static sim::object* spawnFromLevelEditor(render::state* rstate, mm::vec2r position);

		const static std::string name;
		constexpr const static level_editor::level_editor_object_properties properties = level_editor::level_editor_object_properties{0,true};

		mm::rectr getRectangle(unsigned int id) override;
		void setRectangle(unsigned int id, mm::rectr rectangle) override;
		mm::vec2 getSize() override;
		void setSize(mm::vec2 size) override;

		CLASSES_SYNC_POS_AND_VELOCITY(Wall);
		
	private:
		render::object render_object;
		render::state* rstate; //TODO
		mm::rectr shape;

		//GE_TrackMeAsSerializable tracker;
};

class self_replicator : public sim::object 
{
	public:
		self_replicator(render::state* rstate);

		bool C_Update() override;
	private:
		render::state* rstate;
		unsigned long long tick_to_deploy;		

		render::object render_object;

		void set_clock();
};
struct classes_global_info
{

};
*/
