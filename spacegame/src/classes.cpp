//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "classes.h"
/*

extern render::state* rstate; //TODO HACK - this basing off a global in main.cpp

#include <SDL.h>
#include <string>
#include <cmath>
#include <random>

//Local includes
#include "mathematics/primitives.hpp"
#include "mathematics/common_ops.hpp"
#include "UI/minimap.h"
#include "mathematics/raycast.h"
#include "filesystem/filesystem.hpp"
#include "synchro/synchro.hpp"

//Debug includes
#include "render/debug.hpp"

bool typeInGroup[TYPE_COUNT][GROUP_COUNT];

mm::vec2 get_forward_normal_from_angle(double angle)
{
	auto normal = mm::vec2{0,-1};
	normal = mm::rotation_ccw(normal,angle);
	return normal;
}
mm::vec2 get_right_normal_from_angle(double angle)
{
	auto normal = mm::vec2{1,0};
	normal = mm::rotation_ccw(normal,angle);
	return normal;
}
bool could_stop(double distance, double velocity, double acceleration)
{
	double time_destination = distance/(velocity*get_delta_time_for_a_tick());
	double time_stop = velocity / acceleration;
	return time_stop < time_destination;
}
bool could_stop_fixed(double distance, double velocity, double acceleration)
{
	double time_destination = distance/(velocity*get_delta_time_for_a_tick());
	double time_stop = velocity / -acceleration;
	return time_stop < time_destination;
}
double get_angle_difference_to_normal(mm::vec2 target_normal, double current_angle)
{
	double dot_with_right_normal = mm::dot(target_normal,get_right_normal_from_angle(current_angle));
	double dot_with_forward_normal = mm::dot(target_normal,get_forward_normal_from_angle(current_angle));

	double angle = std::acos(dot_with_forward_normal);
	angle *= (dot_with_right_normal > 0)? -1 : 1;
	return angle;
}
double get_target_angular_acceleration(double angle_difference, double angular_velocity, double max_angular_thrust)
{
	double angular_acceleration = max_angular_thrust * ((angle_difference < 0)? -1 : 1); //Accelerate the other way if to the left
	if (angular_velocity * angle_difference > 0) //If angular velocity and current angle have the same sign (i.e. check if we're actually going in the direction of the target angle right now)
	{
		angular_acceleration *= (could_stop(angle_difference, angular_velocity, angular_acceleration))? 1 : -1; // Check if we should stop
	}
	return angular_acceleration;
}
double get_target_scalar_acceleration(double scalar_velocity, double max_thrust_forward, double max_thrust_backward, double scalar_distance)
{
	//bool is_forward_thrust = scalar_distance >= 0;
	printf("fwd %f dist %f vel %f\n",max_thrust_forward, scalar_distance,scalar_velocity);
	bool is_forward_thrust = (max_thrust_forward * scalar_distance) >= 0; //if thrust forward and the distance to travel are the same sign, then it should be forward thrust
	printf("is fwd %d\n",is_forward_thrust);
	//if (scalar_velocity * scalar_distance >= 0) //If angular velocity and current angle have the same sign (i.e. check if we're actually going in the direction of the target angle right now)
	if (scalar_velocity * scalar_distance > 0) //If angular velocity and current angle have the same sign (i.e. check if we're actually going in the direction of the target angle right now)
	{
		printf("should stop?\n");
		is_forward_thrust = (could_stop_fixed(scalar_distance, scalar_velocity, is_forward_thrust? max_thrust_backward : max_thrust_forward))? is_forward_thrust : !is_forward_thrust; // Check if we should stop
	} // if not, we are already going to return a value for them to slow down.
	printf("is fwd final %d\n",is_forward_thrust);
	return is_forward_thrust? max_thrust_forward : max_thrust_backward;
}
double get_scalar_velocity_towards_target(mm::vec2 current_velocity, mm::vec2 current_position, mm::vec2 target_position)
{
	mm::vec2 normal_line_towards_target = mm::unit_vector(target_position - current_position);
	//we are exclusively interested in our velocity that is towards the target. 
	//therefore we obtain our target-bound velocity by getting the dot product from a normal line which points towards the target.
	double scalar_velocity = mm::dot(static_cast<mm::vec2>(current_velocity), normal_line_towards_target);
	return scalar_velocity;
}
mm::vec2 get_target_relative_acceleration(mm::vec2r current_position, mm::vec2 current_velocity, mm::vec2 max_thrust, mm::vec2 max_reverse_thrust, mm::vec2 target_position) 
{
	mm::vec2 normal = {0,1};
	normal = mm::rotation_ccw(normal,current_position.r);
	mm::vec2 right_normal = GE_Normal(normal);

	mm::vec2 distance_raw = static_cast<mm::vec2>(target_position-current_position);
	//distance_raw.x *= -1;
	mm::vec2 distance = 
	{
		-mm::dot(distance_raw,right_normal),
		mm::dot(distance_raw,normal)
	};
	mm::vec2 rotated_velocity =
	{
		-mm::dot(current_velocity,right_normal),
		mm::dot(current_velocity,normal)
	};

	mm::vec2 acceleration;
	acceleration.x = get_target_scalar_acceleration(rotated_velocity.x,max_thrust.x,max_reverse_thrust.x,distance.x);
	acceleration.y = get_target_scalar_acceleration(rotated_velocity.y,max_thrust.y,max_reverse_thrust.y,distance.y);
	return acceleration;
}
auto signof(auto in)
{
	return in > 0.0? 1.0 : -1.0;
}
auto abs_min(auto a, auto b)
{
	return (std::abs(a) < std::abs(b))? a : b;
}


/*!
 *
 * NOTE:
 *
 * On most constants for velocity, you will see something like this:
 * constantvar = constant#*60*60
 *
 * And when they are used:
 * constantvar*get_delta_time_for_a_tick()
 *
 * The reasoning for the first *60 is because the engine used to always treat velocity in units per tick. 
 * A tick used to only be 1/60 seconds, thus units / (1/60th a second). However, velocity is now
 * measured in units per second, and thus constant where 60 times too small.
 *
 * The last *60, and the new *get_delta_time_for_a_tick() are because the engine used to be locked
 * to 60 ticks, and would call Update() on each object 60 times per second. However, now that
 * tick rate is variable, the ammount of velocity that should be added on each tick is dependant
 * on how much time has elasped in this tick. Therefore, the constant is multiplied by 
 * get_delta_time_for_a_tick() The old constants where set without
 * accounting for this, thus they need to be multiplied by 60 first. If the engine is running
 * at 60tps, then the two will cancel out -- 60 * (1/60)
 */


//Our systems instantiate their type mixtures.


class modular_subsystem_system : public sim::system
{
	sim::physen& physics;

	container::type_mixture modular_subsystem;

	public:
		modular_subsystem_system(sim::physen& physics, engine_state& estate) :
			physics(physics),
			modular_subsystem(physics.type_monarch, std::vector<container::lease_info const *>
			{

			}, physics.minimal_physics_object)
		{
		}
		~modular_subsystem_system() override final {} 
		void tick() override final
		{
		}
};


struct spacegame_ecs_data
{
	sim::physen& physics;
	engine_state& estate; 
	render::render_engine& rengine;
};

spacegame_ecs::spacegame_ecs(sim::physen& physics, render::render_engine& rengine, engine_state& estate) :
	data(std::make_unique<spacegame_ecs_data>
	(
	 	physics,
		estate,
		rengine
	))
{
	physics.systems.push_back(std::make_unique<modular_subsystem_system>(physics,estate));
}
spacegame_ecs::~spacegame_ecs()
{
}
size_t spacegame_ecs::make_wall(mm::vec2r position)
{
	size_t id = data->physics.minimal_physics_object.create_entity();
	data->physics.create_physics_object(id, position, mm::vec2r{0,0,0}, 10);
	data->rengine.render_object_lease.component_map.push_back_at
	(
	 	id,
		render::object
		(
		 	data->estate,
			"gray",
			position,
			mm::vec2{50,50},
			mm::rect{0,0,1,1}
		)
	);

	return id;
}



/*


Subsystem::Subsystem(render::state* rstate, std::string sprite, mm::vec2 size, mm::rect animation, mm::vec2r relativePosition, int collisionRectangle, std::string name, mm::vec2* parrentGrid) :
	name(name),
	render_object(rstate, sprite, {0,0,0}, size, animation),
	relativePosition(relativePosition),
	collisionRectangle(collisionRectangle)
{
	this->spriteName = sprite;

	this->health = 100;

	this->level = 0; //TODO

	this->parrentGrid = parrentGrid;
}
void Subsystem::CheckCollision(int checkCollisionRectangle)
{
	if (checkCollisionRectangle == this->collisionRectangle)
	{
		health -= 25; //TODO more dynamicness
		if (!GetIsOnline())
		{
			//Re-construct the render object
			//
			//In the future, this should probably be refactored to be an animation instead, because this is unwieldy and doesn't make much sense.
			//It is, however, well-defined behavior. Just ugly.

			mm::vec2r position = render_object->position;
			mm::vec2 size = render_object->size;
			mm::rect animation = render_object->animation;
			render::state* rstate = render_object->rstate;

			render_object.~object_tracker();
			new (&render_object) render::object_tracker(rstate,spriteName+"_broken",position,size,animation);
		}
	}
}
void Subsystem::Update(mm::vec2r parrentPosition)
{
	double halfrectw = parrentGrid->x/2;
	double halfrecth = parrentGrid->y/2;
	//printf("x %f\n",(halfrectw));
	mm::vec2r rotationMatrix = relativePosition;

	rotationMatrix.x = (rotationMatrix.x)+(render_object->size.x/2)-halfrectw;
	rotationMatrix.y = (rotationMatrix.y)+(render_object->size.y/2)-halfrecth;
	rotationMatrix = mm::rotation_ccw(rotationMatrix,parrentPosition.r);
	render_object->position = {rotationMatrix.x+parrentPosition.x,rotationMatrix.y+parrentPosition.y,parrentPosition.r}; 
}
bool Subsystem::GetIsOnline() const
{
	return (health > 0);
}
int Subsystem::GetLevel() const
{
	return level;
}
void Subsystem::SetLevel(int level)
{
	this->level = level;
}

namespace action
{
	manager_t manager("action");
	thread_local event::endpoint_interface<event_configuration_types> interface(manager);
	std::optional<netplay::network_interface<event_configuration_types>> netif;
}
namespace new_player
{
	manager_t manager("new_player");
	thread_local interface_t interface(manager);
}


void ShootBullet(render::state* rstate, sim::object* host, mm::vec2r addToVelocity, mm::vec2r addToPosition, bool isPlayer)
{
	addToPosition.x = addToPosition.x;// - host->collision_info.bounding_box.x/2;
	addToPosition.y = addToPosition.y;// - host->collision_info.bounding_box.x/2;

	addToPosition = mm::rotation_ccw(addToPosition,host->position.r);
	addToVelocity = mm::rotation_ccw(addToVelocity,host->position.r);

	addToPosition.x = addToPosition.x;// + host->collision_info.bounding_box.x/2;
	addToPosition.y = addToPosition.y;// + host->collision_info.bounding_box.x/2;

	StdBullet* mybullet = new StdBullet(rstate,host->position+addToPosition,(isPlayer) ? ("stdBulletPlayer") : ("stdBulletEnemy"));

	addToVelocity.x += host->velocity.x; //avoid use of vector addition because we don't want the rotation to be added.
	addToVelocity.y += host->velocity.y;

	mybullet->velocity = mybullet->velocity + addToVelocity; 
}

const std::string Player::name = "Player";

Player::Player(synchro::Snapshot FUTURE_AUTO snapshot) :
	Player(snapshot.global_info.rstate, synchro::get<&Player::my_controlling_player>(snapshot), snapshot.global_info.controls)
{
	snapshot.apply(this);
}

#define initialize_axis_interface(interf) interf ## _interface(controls.axes[player_control_axes:: interf ] )
#define initialize_button_interface(interf) interf ## _interface(controls.buttons[player_control_buttons:: interf ] )


//deprecated macros:
#define sizePlusDoubleSize(x,y)  {x*2,y*2}, {0,0,x,y}
#define positionDouble(x,y) {x*2,y*2,0}
//the above inserts TWO PARAMETERS
Player::Player(render::state* rstate, netplay::client_id_t controlling_player_id, player_controls& controls) : 
	sim::object({0,0,0},{0,0,0},25),
	initialize_axis_interface(forward),
	initialize_axis_interface(perpendicular),
	initialize_axis_interface(angular),
	initialize_button_interface(shoot),
	initialize_button_interface(dampen),
	my_controlling_player(controlling_player_id),
	rstate(rstate),
	//TODO - When created not in the physics thread, this will bind to the wrong thread interface
	scancode_listener(scan_code_interface, [this,rstate](input::computer::scan_code::event_data_t in)
	{
		if (in.scan_code < sizeof(keysHeld))
		{
			keysHeld[in.scan_code] = in.is_down;
		}
		
		if (in.is_down)
		{
			else if (in.scan_code == SDL_SCANCODE_4)
			{
				auto new_replicator = new self_replicator(rstate);
				new_replicator->position = position + mm::vec2{200,200};
			}
		}
	},{}),
	mouse_button_listener(mouse_button_interface, [this](auto in)
	{
		if(in.button == input::computer::mouse_button::button_t::left)
		{
#if DEBUG_RENDERS_ENABLED()
			debug::draw_rect<debug::physics_position_modifier>(debug::sticky_object_pusher{25.0},mm::rect{in.location.x,in.location.y,2,2},{0x00,0xff,0x00,0x00});
#endif

			target_position = in.location;
		}
	},{})
{
	
	this->rstate = rstate;

	
	//TODO: Horrible spahgetti. At some point, create a better modular base for Player / completely redo initialization phase
	

	collision_info.addCollisionRectangle(mm::rectr{18,0,0,10,102});
	collision_info.addCollisionRectangle(mm::rectr{28,26,0,42,52}); 
	collision_info.addCollisionRectangle(mm::rectr{0,30,0,4,38}); 
	collision_info.addCollisionRectangle(mm::rectr{90,30,0,4,38}); 
	collision_info.addCollisionRectangle(mm::rectr{70,0,0,10,102}); 
	collision_info.addCollisionRectangle(mm::rectr{28,70,0,42,22}); 
	collision_info.addCollisionRectangle(mm::rectr{28,4,0,42,22});  //~~
	collision_info.addCollisionRectangle(mm::rectr{10,42,0,9,18});  
	collision_info.addCollisionRectangle(mm::rectr{80,42,0,9,18});  

	callCallbackUpdate = true;
	callCollisionCallback = true;

	iterableSubsystems.push_back(Subsystem(rstate,"playerThruster",sizePlusDoubleSize(5,51),positionDouble(9,0),0,"Left Thruster",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerCoreReactor",sizePlusDoubleSize(21,26),positionDouble(14,13),1,"Core Reactor",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerLShield",sizePlusDoubleSize(9,19),positionDouble(0,15),2,"Left Shield",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerRShield",sizePlusDoubleSize(9,19),positionDouble(39,15),3,"Right Shield",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerThruster",sizePlusDoubleSize(5,51),positionDouble(35,0),4,"Right Thruster",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerLifeSupport",sizePlusDoubleSize(21,14),positionDouble(14,35),5,"Life Support",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerShipHead",sizePlusDoubleSize(21,11),positionDouble(14,2),6,"Bridge",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerLSmallTurret",sizePlusDoubleSize(4,9),positionDouble(5,21),7,"Left Turret",&collision_info.bounding_box));
	iterableSubsystems.push_back(Subsystem(rstate,"playerRSmallTurret",sizePlusDoubleSize(4,9),positionDouble(40,21),8,"Right Turret",&collision_info.bounding_box));


	nextTickCanShoot = 0;

	iterableSubsystems[0].SetLevel(4);

	type = TYPE_PLAYER;

	dampeners = false;

	C_Update();


	become_tracked();

	new_player::interface.push_event({this, controlling_player_id});
	new_player::interface.send_events();
	new_player::interface.dispatch_events();
}
sim::object* Player::spawnFromLevelEditor(render::state* rstate, mm::vec2r position)
{
	/* //TODO
	auto myplayer = static_cast<sim::object*>(new Player(rstate,netplay::my_client_id)); //TODO
	myplayer->position = position;
	myplayer->C_Update();
	return myplayer;
	*/
/*
}
void Player::setPosition(mm::vec2r position)
{
	this->position = position;
	C_Update();
}






class tester : public sim::object
{
	public:
		tester(render::state* rstate,mm::vec2r position) : sim::object(position,{0,0,0},25)
		{
			renderObject = GE_CreateRenderedObject(rstate,"simple"); 
			renderObject->size = {8*2,9*2};
			renderObject->animation = {0,0,8,9};
			collision_info.addCollisionRectangle(mm::rectr{0,0,0,8*2,9*2});

			GE_LinkVectorToPhysicsObjectPosition(this,&(renderObject->position)); 

			this->type = TYPE_RESERVED;

			tracker = GE_AddTrackedObject(type,this);


			velocity.x = 1;

		}
		void C_Destroyed()
		{
			GE_RemoveTrackedObject(tracker);
			GE_FreeRenderedObject(renderObject);
		}

	private:
		GE_RenderedObject* renderObject;
		GE_TrackedObject* tracker;


};
*/


/*



//old constants set when engine tickrate was always 60 (60 calls to update() per second, plus velocity was in units per 1/60th a second)
const double turnSpeed= 0.0008726646*60*60;
const double moveSpeed = 0.25*60*60;
const double strafeSpeed = 0.05*60*60;




bool Player::C_Update()
{
	forward_interface.dispatch_events();
	perpendicular_interface.dispatch_events();
	angular_interface.dispatch_events();
	shoot_interface.dispatch_events();
	dampen_interface.dispatch_events();

	action::netif->send_events();
	action::netif->dispatch_events();

	scan_code_interface.dispatch_events();
	mouse_button_interface.dispatch_events();
	//Update subsystem positions
		

	for (size_t i=0;i < iterableSubsystems.size();i++)
	{
		iterableSubsystems[i].Update(position);
		if (!iterableSubsystems[i].GetIsOnline())
		{
			collision_info.collisionRectangles[i] = {0,0,0,0,0};
		}
	}
	//TODO temp
	//renderObject->position = this->position;


	if (GetIsOnline())
	{
		if(keysHeld[SDL_SCANCODE_X])
		{
			printf("posx %f ",position.x);
			printf("posy %f ",position.y);
			printf("r %f\n",position.r);
			printf("velx %f vely %f velr %f\n",velocity.x,velocity.y,velocity.r);
		}
		if(keysHeld[SDL_SCANCODE_P])
		{
			//iterableSubsystems[0].health = 0;
			//iterableSubsystems[0].CheckCollision(0);
			velocity = {0,0,0};
		}
		if (keysHeld[SDL_SCANCODE_G])
		{
			mm::vec2 target_normal = mm::unit_vector(target_position - position());

			double angle = get_angle_difference_to_normal(target_normal, position().r);

			double angular_acceleration = turnSpeed*get_delta_time_for_a_tick();
			velocity.r += get_target_angular_acceleration(angle, velocity.r, angular_acceleration);

			//accelerate towards the player
			mm::vec2 max_accel = mm::vec2{strafeSpeed,-moveSpeed}*get_delta_time_for_a_tick();
			mm::vec2 acceleration = get_target_relative_acceleration(position(), static_cast<mm::vec2>(velocity), max_accel,-max_accel, target_position);
			printf("final accel %f %f\n",acceleration.x, acceleration.y);
			GE_AddRelativeVelocity(this,static_cast<mm::vec2r>(acceleration));
		}

		float accel_fwd = *forward_axis;
		float accel_perp = *perpendicular_axis;
		float accel_ang = *angular_axis;

		if (*dampen_button)
		{
			//TODO proof of concept implementation
			auto target_normal = mm::unit_vector(-velocity);
			double dot_with_right_normal = mm::dot(target_normal,get_right_normal_from_angle(position().r));
			double dot_with_forward_normal = mm::dot(target_normal,get_forward_normal_from_angle(position().r));

			auto relative_velocity = velocity;
			relative_velocity = mm::rotation_cw(relative_velocity, position().r);

			double angle = std::acos(dot_with_forward_normal) * -signof(dot_with_right_normal);
			
			accel_ang = 1.0;
			//then try to angle ourselves in line with the direction we're trying to thrust in
			//(for the maximum thrust)
			accel_ang *= (dot_with_right_normal > 0)? -1 : 1; //Accelerate the other way if to the left

			if (velocity.magnitude() < moveSpeed * 0.0625) // if the velocity could be dampened in 1/16 a second or less (it may actually take longer if orientation is not ideal)
			{
				//then just dampen our angular acceleration
				accel_ang = -std::min(velocity.r / (turnSpeed*get_delta_time_for_a_tick()), signof(velocity.r));
			}
			if (velocity.r * angle > 0) //If angular velocity and current angle have the same sign (i.e. check if we're actually going in the direction of the target angle right now)
			{
				//then try to angle ourselves in line with the direction we're trying to thrust in
				//(for the maximum thrust)
				accel_ang *= (could_stop(angle, velocity.r, turnSpeed * accel_ang * get_delta_time_for_a_tick()))? 1 : -1; // Check if we should stop
			}

			//TODO combine these into a singular function that isn't purpose-built for when moveSpeed > strafeSpeed
			

			accel_perp = std::clamp(-relative_velocity.x / (strafeSpeed*get_delta_time_for_a_tick()), -1.0,1.0);
			accel_fwd =  std::clamp(-relative_velocity.y / (moveSpeed  *get_delta_time_for_a_tick()), -1.0,1.0);
			//printf("fwd %f\n",accel_fwd);
			//printf("perp %f\n",accel_perp);
			//printf("finish %f\n",-relative_velocity.x / (strafeSpeed*get_delta_time_for_a_tick()));
		}


		//if (iterableSubsystems[0].GetIsOnline()) GE_AddRelativeVelocity(this,{0, moveSpeed * (accel_fwd) * get_delta_time_for_a_tick(),-turnSpeed*get_delta_time_for_a_tick()});
		//if (iterableSubsystems[4].GetIsOnline()) GE_AddRelativeVelocity(this,{0, moveSpeed * (accel_fwd) * get_delta_time_for_a_tick(),turnSpeed*get_delta_time_for_a_tick()});
		GE_AddRelativeVelocity(this,{0, moveSpeed * (accel_fwd) * get_delta_time_for_a_tick(),0});
		//	printf("perp %f\n",accel_perp);
		GE_AddRelativeVelocity(this,{strafeSpeed * (accel_perp) * get_delta_time_for_a_tick(),0,0});
		velocity = velocity + mm::vec2r{0,0,turnSpeed * (accel_ang) * get_delta_time_for_a_tick()};
		if (*shoot_button && ticknum >= nextTickCanShoot && netplay::net_state != netplay::state::client)
		{
			//TODO at some point this should be based on a value made in an editor of some sort.
			if (iterableSubsystems[7].GetIsOnline()) ShootBullet(this->rstate,this,{0,(-10*60),0},{12-(collision_info.bounding_box.x/2),-60,0},true);
			if (iterableSubsystems[8].GetIsOnline()) ShootBullet(this->rstate,this,{0,(-10*60),0},{86-(collision_info.bounding_box.x/2),-60,0},true); //(49*2)-12=86 , because the other turret is at the opposite side of the ship
			nextTickCanShoot = ticknum +(0.5)/get_delta_time_for_a_tick()  /( (30-(pow(iterableSubsystems[0].GetLevel(),2))) *60*get_delta_time_for_a_tick());
		}
	}
	//debug::textAt(std::to_string(position().r),mm::vec2{0,0});
	return false;
}
bool Player::C_Collision(sim::object* victim, int collisionRectangleID)
{
	//get physics object
	
	//is it a bullet?
	if (victim->type == TYPE_DESTROYSUB)
	{	
		//What subsytem did it hit?
		for (size_t i = 0;i <iterableSubsystems.size();i++)
		{
			iterableSubsystems[i].CheckCollision(collisionRectangleID);
		}


	}


	return false;
}
bool Player::GetIsOnline()
{
	return (iterableSubsystems[1].GetIsOnline() && iterableSubsystems[5].GetIsOnline() && iterableSubsystems[6].GetIsOnline());
}




Enemie::Enemie(render::state* rstate, mm::vec2r position, int level) : sim::object(position,{0,0,0},25),render_object(rstate, "enemy", position,{38,42},{0,0,19,21})  //,tracker(TYPE_ENEMY,this)
{
	this->rstate = rstate;

	this->level = level;

	type = TYPE_ENEMY;
	
	//renderObject = GE_CreateRenderedObject(rstate,"enemy"); 
	//renderObject->size = {38,42};
	//renderObject->animation = {0,0,19,21};
	
	collision_info.addCollisionRectangle(mm::rectr{0,0,0,38,42}); 

	
	callCollisionCallback = true;
	callCallbackUpdate = true;
	last_shot_tick = ticknum;

	become_tracked(); 
}
Enemie::~Enemie()
{
	//GE_ScheduleFreeMinimapTarget(renderObject); //TODO
}
bool Enemie::C_Update()
{
	render_object->position = position();


	//TODO: Refactor engine control code into its own seperate class; only focus on pointing that class where to go here.

	std::vector<sim::object*> potential_block;
	std::unordered_set<sim::object*> potential_target;
	
	//find stuff in our radius
	for (sim::object* obj : GE_GetObjectsInRadius({position().x,position().y},1000))
	{
		if (obj != this)
		{
			if (obj->type == TYPE_PLAYER) 
			{
				 potential_target.insert(potential_target.end(),obj);
			}
			potential_block.insert(potential_block.end(), obj);
		}
	}

	std::optional<mm::vec2r> target_position;
	double keep_distance_from_target;
	std::optional<mm::vec2> target_normal;
	std::optional<sim::object*> target_player; 

	//Raycast and see if we can see the player
	if (!potential_target.empty())
	{
		struct currently_selected_player_t
		{
			sim::object* player;
			double distance_from_me;
		};
		std::optional<currently_selected_player_t> currently_selected_player;
		for (auto test_player : potential_target)
		{
			//we're only worried about our sight to the player.
			mm::vec2 end_raycast_position = {test_player->position().x,test_player->position().y}; 
			auto raycast_result = GE_Raycast(static_cast<mm::vec2>(position()),end_raycast_position,potential_block);

#if DEBUG_RENDERS_ENABLED()
			debug::draw_line<debug::physics_position_modifier>(debug::transient_object_pusher{},static_cast<mm::vec2>(position()),end_raycast_position,GE_Color{0xff,0x00,0x00,0x00});
			debug::draw_rect<debug::physics_position_modifier>(debug::transient_object_pusher{},mm::rect{raycast_result.position.x,raycast_result.position.y,2,2},{0x00,0xff,0x00,0x00});

			debug::draw_line<debug::physics_position_modifier>(debug::transient_object_pusher{},static_cast<mm::vec2>(position()),static_cast<mm::vec2>(position()) + (get_forward_normal_from_angle(position().r)*20.0),GE_Color{0xff,0xff,0x00,0x00});
#endif
			if (raycast_result.victim == test_player)
			{
				auto distance_from_me = mm::distance(position(), raycast_result.position);
				if (!currently_selected_player.has_value() || distance_from_me < currently_selected_player->distance_from_me)
				{
					currently_selected_player = {test_player,distance_from_me};
					lastFoundPlayer = raycast_result.position;
				}
			}
		}
		if (currently_selected_player.has_value())
		{
			target_position = currently_selected_player->player->position();
			keep_distance_from_target = 100 + currently_selected_player->player->collision_info.radius;
			target_normal = mm::unit_vector(currently_selected_player->player->position ()- position());
			auto distance_to_player = mm::distance(position(),currently_selected_player->player->position());
			target_player = currently_selected_player->player;

			//GE_LinkMinimapToRenderedObject(renderObject,{0xA0,0x00,0x00,0xFF});
			if (distance_to_player > 3000)
			{
				//GE_ScheduleFreeMinimapTarget(renderObject); //TODO
			}
		}
	}
	else
	{
		if (velocity.magnitude() <= 0.1)
		{
			
		}
		else
		{
			target_normal = mm::unit_vector(-velocity);
		}
	}
	constexpr double myaccel = 0.1f*60;
	double angle;
	if (target_normal.has_value())
	{
		angle = get_angle_difference_to_normal(*target_normal, position().r);
	}


	if (target_player.has_value())
	{
		if (ticknum-last_shot_tick >= (10*60*60*get_delta_time_for_a_tick()))
		{	
			if (std::abs(angle) < 0.3)
			{
				printf("shoot\n");
				last_shot_tick = ticknum;
				//ShootBullet(rstate,this,{0,(-7*60),0},{0,(-collision_info.bounding_box.y/2)-5.0,0},false);
			}
		}
	}
	else
	{
		//No target player; we are braking off all our velocity
		if (std::abs(angle) < 0.3 && velocity.magnitude() > 0.1)
		{
			GE_AddRelativeVelocity(this,{0,-(std::min(velocity.magnitude(), myaccel)),0});
		}
	}

	//TODO: Currently this can "orbit" the player. Fix that.
	if(target_normal.has_value())
	{
		double angular_acceleration = 0.002*60;
		velocity.r += get_target_angular_acceleration(angle, velocity.r, angular_acceleration);
		//velocity.r += get_target_scalar_acceleration(velocity.r,angular_acceleration,-angular_acceleration,angle);

		if (target_position.has_value())
		{
			//accelerate towards the player
			mm::vec2 real_target = static_cast<mm::vec2>(*target_position) - ((*target_normal) * keep_distance_from_target);
			mm::vec2 max_accel = mm::vec2{myaccel*0.1f,-myaccel};
			mm::vec2 acceleration = get_target_relative_acceleration(position(), static_cast<mm::vec2>(velocity), max_accel,-max_accel, real_target);
			GE_AddRelativeVelocity(this,static_cast<mm::vec2r>(acceleration));
		}
	}
	return false;
}






const std::string Enemie::name = "Enemy";
bool Enemie::C_Collision(sim::object* victim, int collisionRectangleID)
{
	//SDL_Delay(1600);
	//get physics object
	
	//is it a bullet?
	if (victim->type == TYPE_DESTROYSUB)
	{
		//bye
		return true;
	}
		

	return false;
}
sim::object* Enemie::spawnFromLevelEditor(render::state* rstate, mm::vec2r position)
{
	return static_cast<sim::object*>(new Enemie( ::rstate,position,0));
}


BulletType::BulletType(mm::vec2r position, mm::vec2r velocity, double mass) : sim::object(position,velocity, mass)
{
	type = TYPE_DESTROYSUB;
}
bool BulletType::C_Collision(sim::object* victim, int collisionRectangleID)
{
	//printf("Collision! I should be dead now!\n");
	return true; //Tell the physics engine to delete me.
}


StdBullet::StdBullet(render::state* rstate, mm::vec2r position, std::string spriteName) : BulletType(position,{0,0,0},STD_BULLET_MASS), render_object(rstate, spriteName, position, {2,10}, {0,0,1,5})
{
	this->spriteName = spriteName;

	collision_info.addCollisionRectangle(mm::rectr{0,0,0,4,10}); 
	
	callCollisionCallback = true;


	callCallbackUpdate = true; //TODO done until a better solution is made for updating render_object position
	become_tracked();
}
bool StdBullet::C_Update()
{
	render_object->position = position();
	return false;
}

const std::string Wall::name = std::string("Wall");
Wall::Wall(render::state* rstate, mm::vec2r position, mm::rectr shape, double mass) : sim::object(position,mm::vec2r{0,0,0},mass), //,tracker(TYPE_SHIPWALL,this)
	render_object(rstate,"gray", position, {shape.w, shape.h},{0,0,1,1})
{
	this->shape = shape;
	this->collision_info.addCollisionRectangle(shape);
	
	type = TYPE_SHIPWALL;
	
	callCollisionCallback = true;

	//GE_InelasticCollision(this,mm::vec2{position.x,position.y}+mm::vec2{shape.w,shape.h},mm::vec2{0,mass*2},false);
	become_tracked();

	//TODO done until there's a better solution for updating render_object
	callCallbackUpdate = true;
}
bool Wall::C_Collision(sim::object* victim, int collisionRectangleID)
{
	velocity = {0,0,0};
	return false;
}

bool Wall::C_Update()
{
	render_object->position = position();
	
	return false;
}


sim::object* Wall::spawnFromLevelEditor(render::state* rstate, mm::vec2r position)
{
	return static_cast<sim::object*>(new Wall(rstate,position,mm::rectr{0,0,0,100,10},10)); 
}
mm::rectr Wall::getRectangle(unsigned int rect)
{
	auto collisionRect = collision_info.collisionRectangles[0];
	return collisionRect;
}
mm::vec2 Wall::getSize()
{
	auto rectangle = collision_info.collisionRectangles[0];
	return {rectangle.w,rectangle.h};

}
void Wall::setSize(mm::vec2 size)
{
	collision_info.collisionRectangles[0].w = size.x;
	collision_info.collisionRectangles[0].h = size.y;
	this->collision_info.refresh_collision_rectangle_cache();

	render_object->size = size;
}
void Wall::setRectangle(unsigned int rect, mm::rectr value)
{
	collision_info.collisionRectangles[0] = value;
	render_object->size = {value.w,value.h};
}


unsigned long long num_replicators = 0;

const double replicator_mass = 10;
self_replicator::self_replicator(render::state* rstate) : sim::object({0,0,0},mm::vec2r{0,0,0},replicator_mass), rstate(rstate),
	render_object(rstate, "replicator", {0,0,0}, {38,38}, {0,0,19,19})
{
	num_replicators++;
	if ((num_replicators % 1000)==0)
	{
		printf("Num replicators %llu\n",num_replicators);
	}

	auto image_size = mm::vec2{19,19};

	this->collision_info.addCollisionRectangle({0,0,0,image_size.x*2,image_size.y*2});

	callCallbackUpdate = true;

	set_clock();
}
void self_replicator::set_clock()
{
	double seconds_to_deploy = 1.0;
	tick_to_deploy = ticknum + seconds_to_deploy*(1.f / get_delta_time_for_a_tick());
}
bool self_replicator::C_Update()
{
	render_object->position = position();
	if (ticknum == tick_to_deploy)
	{
		auto new_replicator = new self_replicator(rstate);

		std::default_random_engine random_engine(std::random_device{}());
		std::uniform_real_distribution<double> dist(-100.0,100.0);

		auto random_vec = mm::vec2{dist(random_engine), dist(random_engine)};
		new_replicator->position() = position() + mm::vec2{40,40}+random_vec;
		new_replicator->velocity = velocity;

		set_clock();
	}
	return false;
}




std::promise<size_t> player_center_render_object_id_promise;


std::optional<event::listener<netplay::new_name_event::interface_t>> new_client_listener;
void InitClasses(render::state* rstate, player_controls& controls)
{
	action::netif = netplay::network_interface<action::event_configuration_types>(action::manager);

	typeInGroup[TYPE_PLAYER][GROUP_INTELIGENT] = true;//some players shouldn't be in this, but we'll overlook that
	typeInGroup[TYPE_REGULAR][GROUP_REGULAROBJECT] = true;

	level_editor::register_class_with_level_editor<Wall>(TYPE_SHIPWALL);
	level_editor::register_class_with_level_editor<Enemie>(TYPE_ENEMY);
	level_editor::register_class_with_level_editor<Player>(TYPE_PLAYER);

	new_client_listener = event::listener(*netplay::new_name_event::netplay_thread_interface, [=, &controls](auto in)
	{
		auto my_player = new Player(rstate,in.event_data.id, controls); 
		player_center_render_object_id_promise.set_value(my_player->iterableSubsystems[1].render_object.id);
	}, {});

}

void ShutdownClasses()
{
	new_client_listener.reset();	
}
*/
