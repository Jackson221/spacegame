/*
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
*/



/*!
 * This might not necessarily be the best place to start if you are trying to figure out how Module2D works
 * 		Though it may be helpful for a broad overview and example usage of Module2D
 *
 * You might try looking at src/simulation/physics.cpp
 * or for game-specific logic, classes.cpp
 *
 * For netcode, you'll want to look at src/event/event.hpp & src/simulation/netplay.cpp & hpp
 *
 * For UI, look into src/UI/UI.cpp
 */


#include <SDL.h>
#include <SDL_ttf.h>
#include <chrono>
#include <cstdio>
#include <mutex>
#include <cmath>

#include <memory>
#include <vector>
#include <functional>
#include <string>
#include <iostream>
#include <cmath>
#include <random>

//Local includes
#include "mathematics/primitives.hpp"
#include "render/camera.hpp"
#include "render/sprite.hpp"
#include "simulation/physics.hpp"
#include "render/object.hpp"
#include "vkrender/init.hpp"
#include "UI/UI.hpp"
#include "UI/minimap.h"
#include "engine.hpp"
#include "render/stars.hpp"
#include "UI/shapes.h"
#include "render/game_render.hpp"
#include "UI/font.h"
#include "filesystem/filesystem.hpp"
#include "event/event.hpp"
#include "network/network.hpp"
#include "input/interfaces.hpp"
#include "UI/chat.hpp"
#include "simulation/netplay.hpp"
#include "container/bst.h"
#include "render/debug.hpp"
#include "cli/flag_interpreter.hpp"
#include "options/options.hpp"
#include "variadic_util/variadic_util.hpp"
#include "worldinput/mouse.hpp"
#include "synchro/synchro.hpp"
#include "algorithm/frame.hpp"

//Game-specific
//#include "level_editor.hpp"
#include "classes.h"

//Config


//have to make the star plate bigger; little doge cpus can't handle it
const double max_zoom_out = std::thread::hardware_concurrency() >= 16? 0.0625 : 0.125;
const double max_zoom_in = 99.0;


const unsigned short SERVER_DEFAULT_PORT = 7069;

/*!
 * config
 */
ui::menu_list_style right_click_style;
ui::menu_list_style my_menu_list_style;
ui::font_style_t fstyle;
ui::window_title_style_t window_title_style;
ui::window_style_t window_style;
ui::style style;
ui::color window_background_color_focused = ui::color{0x66,0xff,0x33,0xff};
ui::color window_background_color_unfocused = ui::color{0xa4,0xf4,0x88};
//auto menu_button_style = ui::button_style_t{ui::color{0x0f,0x33,0x11,0xff}/*{0x00,0x33,0x00,255}*/,ui::color{0x55,0xee,0x22,0xff},ui::color{0x1e,0x88,0x1a,0xff}};

auto menu_button_sprite_style_background = ui::sprite_style_t{"button_background",{255,255,255,255},{0,0,58,19}};
auto menu_button_sprite_style_highlighted = ui::sprite_style_t{"button_highlighted",{255,255,255,255},{0,0,58,19}};
auto menu_button_sprite_style_pressed = ui::sprite_style_t{"button_pressed",{255,255,255,255},{0,0,58,19}};
auto menu_button_style = ui::button_style_sprite_t{menu_button_sprite_style_background, menu_button_sprite_style_highlighted, menu_button_sprite_style_pressed};//ui::color{100,100,100,100},ui::color{200,200,200,100}};

ui::font_style_t menu_button_font;

double default_camera_scale = 0.75;

void init_config()
{
	right_click_style = ui::menu_list_style
	{
		{ui::color{0xff,0xff,0xff,0xff},"FreeSans",15}, //font
		ui::color{0x00,0x33,0x00,255}, //background focused
		ui::color{0x00,0x33,0x00,255}, //background unfocused
		ui::color{0xff,0x00,0x00,255}, //highlight

		ui::color{0x00,0x22,0x00,0xff}, //spacer color
		4.0, //spacer size
		0.9 //coeffecient spacer width
	};
	my_menu_list_style = ui::menu_list_style
	{
		{ui::color{0x00,0x00,0x00,0xff},"FreeSans",35}, //font
		window_background_color_focused, //background
		window_background_color_unfocused, //background
		ui::color{0xff,0x00,0x00,255}, //highlight

		ui::color{0x00,0x22,0x00,0xff}, //spacer color
		4.0, //spacer size
		0.9 //coeffecient spacer width

	};

	//initialize styling
	fstyle = {ui::color{0x00,0x00,0x00,0xff},"FreeSans",18};
	//TTF_SetFontStyle(fstyle.font.font,TTF_STYLE_NORMAL | TTF_STYLE_BOLD);

	window_title_style = {fstyle,0,{ui::color{0xff,0x00,0x00,0xff},ui::color{0x66,0x66,0x66,0xff},{0x33,0x33,0x33,0xff}},fstyle,2.5,2,ui::color{0x66,0xff,0x33,0xff},25,true};
	window_style = {window_title_style, ui::color{0x00,0x0a,0x00,0xff},2,window_background_color_focused, window_background_color_unfocused};
	style = ui::style{fstyle,{ui::color{0x00,0x33,0x00,0xff},"FreeSans",18},window_style};

	menu_button_font = {ui::color{0xff,0xff,0xff,0x00}, "FreeSans",18};

}



#include "test/test.hpp"



/*LONG-TERM TODO

*Lagged camera? ("chasecam") 
*Standard implementation for rotation matrix instead of spamming it everywhere?

*Possible "engine demo game" would be an asteroid field with constant collisions. Would demo the effeciency of the physics engine very well.


*/

//TODO MEGA
sim::physen* global_physics_engine;
render::render_engine* global_render_engine;


namespace {using namespace std::chrono_literals;}
std::chrono::nanoseconds render_ideal_tick_length = 6944444ns;


struct Player
{
	mm::vec2r position;
};

Player* player = NULL; //using a raw pointer here was a mistake -- should've been a weak pointer

size_t player_center_render_object_id;


render::state* rstate;
ui::state uistate;
render::camera camera;
mm::rect camerasGrid;
//TODO OPTIONS
options::options* global_options;

long long unsigned int render_ticknum = 0;
std::chrono::time_point<std::chrono::steady_clock> seconds_passed_timer = std::chrono::steady_clock::now();
auto render_ticknum_144hz_equivilent = [&render_ticknum,&render_ideal_tick_length]()
{
	return render_ticknum * (6944444.0 / static_cast<double>(render_ideal_tick_length.count()));
};
auto render_ticknum_based_seconds_passed = [&render_ticknum,&render_ideal_tick_length]()
{
	return render_ticknum * static_cast<double>(render_ideal_tick_length.count())/(1000000000.0);
};
auto seconds_passed = [&seconds_passed_timer]()
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now()-seconds_passed_timer).count()/(1000000000.0);
};

GE_Font tiny_sans;
GE_Font big_sans;
GE_Font title_sans;


class UI_MainMenu;

struct phosphor_background_style
{
	double const max_phosphor_illuminance;
	double const phosphor_decay_fraction;
	double const max_phosphor_background_variance; //i.e. the percentage that each line will randomly varry in color compared to ground truth.
	
	double const mouse_illuminance_per_frame;

	double const scan_line_illuminance_factor_line;
	double const scan_line_illuminance_factor_trail;
	double const scan_line_advance_coeffecient;
	int const scan_line_height_px;

};

phosphor_background_style default_phosphor_background_style = //constants were set at 144hz before delta_time was introduced so that's why some are multiplied by 144.
{
	.max_phosphor_illuminance = 60,
	.phosphor_decay_fraction = (0.005*144),
	.max_phosphor_background_variance = 0.15,
	
	.mouse_illuminance_per_frame = (5.0*144),

	.scan_line_illuminance_factor_line = 100,
	.scan_line_illuminance_factor_trail = 60,
	.scan_line_advance_coeffecient = (1.0/12.0)*584.0,
	.scan_line_height_px = 2
};
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    constexpr uint32_t rmask = 0xff000000;
    constexpr uint32_t gmask = 0x00ff0000;
    constexpr uint32_t bmask = 0x0000ff00;
    constexpr uint32_t amask = 0x000000ff;
#else
    constexpr uint32_t rmask = 0x000000ff;
    constexpr uint32_t gmask = 0x0000ff00;
    constexpr uint32_t bmask = 0x00ff0000;
    constexpr uint32_t amask = 0xff000000;
#endif

class phosphor_background : public ui::element
{
	public:
		phosphor_background(ui::state uistate, phosphor_background_style _style) :
			ui::element(uistate),
			style(_style)
		{
			positioning_rectangle.set_update_callback_and_run_it_now([this]()
			{
				mm::vec2 size = positioning_rectangle.get_size();
				if (size != last_size && size.y > 0)
				{
					phosphor.resize(size.y);
					SDL_FreeSurface(surface);
					surface = SDL_CreateRGBSurface(0, size.x, size.y, 32, rmask,gmask,bmask,amask);

					signed long delta_size = static_cast<size_t>(size.y)-static_factor.size();
					if (delta_size > 0) //not big enough
					{
						for (signed long i = 0; i < delta_size; i++)
						{
							std::default_random_engine random_engine(std::random_device{}());
							std::uniform_real_distribution<float> fractional_distribution(1.0-style.max_phosphor_background_variance,1.0);
							static_factor.push_back(fractional_distribution(random_engine));
						}
					}
					else if (delta_size < 0)
					{
						static_factor.erase(static_factor.end()-((delta_size*-1)+1), static_factor.end()-1);
					}
					
					last_size = size;
				}
			});
		}
		phosphor_background(phosphor_background const & other) = delete;
		phosphor_background(phosphor_background && other) = delete;
		void impl_render() override
		{
			auto delta_time_time_point = std::chrono::steady_clock::now() - last_render_time;
			double delta_time = std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time_time_point).count() / static_cast<double>(std::nano::den);
			
			mm::vec2 position = positioning_rectangle.get_position();	
			mm::vec2 size = positioning_rectangle.get_size();
			if (size.y < 1)
				return;

			
			//do nothing if delta_size is 0

			for (size_t y = 0; y < static_cast<size_t>(size.y); y++)
			{
				int final_illuminance_factor = 0;

				{ //mouse illuminance
					int mx, my;
					SDL_GetMouseState(&mx,&my); //TODO convert to module2d calls

					int m_dist = std::abs(my - (y+position.y));
					m_dist = (m_dist==0)? 1 : m_dist;

					phosphor[y] += style.mouse_illuminance_per_frame*delta_time/m_dist; 
				}

				{ //scanning illumination line
					//int scan_line_height_px =;
					if ((y > this_illuminance_line-style.scan_line_height_px) && (y < this_illuminance_line + style.scan_line_height_px))
					{
						final_illuminance_factor += style.scan_line_illuminance_factor_line;
						phosphor[y] = style.scan_line_illuminance_factor_trail;
					}
				}

				{ //phosphor clamping, decay, and adding to final illuminance
					phosphor[y] = std::clamp(phosphor[y],0.0,style.max_phosphor_illuminance);
					phosphor[y]  /= 1.0+(style.phosphor_decay_fraction*delta_time);

					final_illuminance_factor += phosphor[y];
				}

				{ //final rendering
					int final_g = std::clamp(final_illuminance_factor,0,255);

					int random_flicker = 0;
					double r = static_factor[y];

					//SDL_SetRenderDrawColor(rstate, random_flicker,r*final_g+random_flicker,random_flicker,255);	
					auto clr = ui::color{random_flicker,random_flicker,r*final_g+random_flicker,255};
					auto rect = SDL_Rect{0,y,size.x,1};
					SDL_FillRect(surface,&rect,SDL_MapRGBA(surface->format,clr.r,clr.g,clr.b,clr.a));
					//cached_colors.at(clr).render({position.x,position.y+y}, {size.x,1});
					//SDL_RenderDrawLine(rstate, static_cast<int>(position.x), static_cast<int>(position.y+y), static_cast<int>(position.x+size.x-0.5),static_cast<int>(position.y+y));
				}
			}
			{ //move the illumination line down & warp back to top if needed
				this_illuminance_line+= delta_time*style.scan_line_advance_coeffecient;

				if (this_illuminance_line >= size.y)
				{
					this_illuminance_line = 0.0;
				}
			}
			auto s = render::backend_sprite(uistate.rstate->api, surface,false);
			uistate.rstate->api.add_transient_object(s, size, {0,0,size.x,size.y},static_cast<mm::vec2r>(position+size/2));
			last_render_time = std::chrono::steady_clock::now();
		}

	private:
		phosphor_background_style style;

		std::vector<double> phosphor;
		std::vector<double> static_factor;

		double this_illuminance_line = 0;
		std::chrono::time_point<std::chrono::steady_clock> last_render_time;

		bool mouse_down = false; //TODO: burn phosphor in more when mouse held

		SDL_Surface* surface = nullptr;
		mm::vec2 last_size = {0,0};
	
};

#if DEBUG_RENDERS_ENABLED()
struct debug_rstate_t
{
	bool render_world;
	bool render_ui;
	bool render_debug;
};
std::vector<debug_rstate_t> const debug_state_array = 
{
//  World   UI   Debug
	{true ,true ,false},
	{true ,true ,true},
	{true ,false,true},
	{false,false,true},
	{true ,false,false},
};
size_t current_debug_state = 0;
double const msg_length = 10;
void bump_current_debug_state()
{
	current_debug_state++;
	if(current_debug_state == debug_state_array.size())
	{
		current_debug_state=0;
	}
	auto interf = debug_state_array[current_debug_state];
	debug::message({0,255,0,255},msg_length,"state %d",current_debug_state);
#define bool_str(in) in? "true" : "false"
	debug::message({0,255,0,255},msg_length,"| render_world: %s | render_ui: %s | render_debug: %s |", bool_str(interf.render_world),bool_str(interf.render_ui),bool_str(interf.render_debug));
#undef bool_str
}
#endif

phosphor_background_style health_view_phosphor_background_style = //constants were set at 144hz before delta_time was introduced so that's why some are multiplied by 144.
{
	.max_phosphor_illuminance = 60,
	.phosphor_decay_fraction = (0.005*144),
	.max_phosphor_background_variance = 0.15,
	
	.mouse_illuminance_per_frame = (5.0*144),

	.scan_line_illuminance_factor_line = 100,
	.scan_line_illuminance_factor_trail = 60,
	.scan_line_advance_coeffecient = 1.0/5.0,
	.scan_line_height_px = 2
};


#define SHAKE_TICKS 15
#define NUM_SHAKES 2.5
#define SHAKE_DISTANCE 5
class UI_HealthView : public ui::element
{
	public:

		UI_HealthView(ui::state uistate,mm::vec2 position, mm::vec2 size, render::camera* camera) : 
			ui::element(uistate)
		{
			positioning_rectangle.set_size(size);
			positioning_rectangle.set_position(position);

			mySurface = std::unique_ptr<ui::surface>(new ui::surface(uistate,ui::color{0x00,0x33,0x00,255}));
			mySurface->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

			auto background = new phosphor_background{uistate,health_view_phosphor_background_style};	
			mySurface->addelement(background);

			numHealthTexts = 0;

			ui::color txt_color = {0xff,0x33,0x33,0xff};


			//TODO
			/*for (int i=0;i<8;i++)
			{
				ui::text* newText = new ui::text(uistate,"This message should've been updated.", txt_color,tiny_sans);
				mySurface->addelement(newText);
				newText->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
				newText->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
				newText->positioning_rectangle.set_origin_position_calculator([this,i](){return positioning_rectangle.get_position()+mm::vec2{positioning_rectangle.get_size().x/2,15*static_cast<double>(i)};});

				healthTexts[numHealthTexts] = newText;
				if (player != NULL)
				{
					is_linked = true;
					//TODO replacement
					//GE_LinkGlueToPhysicsObject(player,GE_addGlueSubject(&healthAmmount[numHealthTexts],&(player->iterableSubsystems[numHealthTexts]->health),GE_PULL_ON_PHYSICS_TICK,sizeof(double)) );
				}
				healthAmmount[numHealthTexts] = 100;
				lastHealth[numHealthTexts] = 100;
				shakeHealthTillTick[numHealthTexts] = -1;
				numHealthTexts++;
			}*/
			speedText = new ui::text(uistate,"This message should've been updated.", txt_color,tiny_sans);
			mySurface->addelement(speedText);

			speedText->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
			speedText->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			speedText->positioning_rectangle.set_origin_position_calculator([this](){return positioning_rectangle.get_position()+mm::vec2{this->positioning_rectangle.get_size().x/2,15*static_cast<double>(numHealthTexts+1)};});

			playerSpeed = {0,0,0};
			if (player != NULL)
			{
				//GE_LinkVectorToPhysicsObjectVelocity(player,&playerSpeed); //TODO
			}

		
		}
		void impl_render() override
		{
			/* //TODO MEGA
			//Note: Shaking text on damage is broken. Probably won't fix until health view is rewritten.
			if (player != NULL)
			{
				for (int i=0;i<numHealthTexts;i++)
				{
					//OK: name is a const value
					sprintf(newStr, "%s: %.2f%%",player->iterableSubsystems[i].name.c_str(),healthAmmount[i]); //name: healthpercentage%
					healthTexts[i]->set_text(newStr);
					
					if (lastHealth[i] != healthAmmount[i])
					{
						shakeHealthTillTick[i] = render_ticknum+SHAKE_TICKS;
					}
					if (shakeHealthTillTick[i] >= render_ticknum)
					{
						int ticksElasped = (render_ticknum+SHAKE_TICKS)-shakeHealthTillTick[i];
						const double oneRevolution = 2*M_PI;

						healthTexts[i]->positioning_rectangle.set_position({(size.x/2)+sin( (ticksElasped*oneRevolution*NUM_SHAKES)/SHAKE_TICKS )*SHAKE_DISTANCE  ,  15*static_cast<double>(i)});	
					}
					
					lastHealth[i] = healthAmmount[i];
				}
				sprintf (newStr, "Speed: %.2f km/s",(abs(playerSpeed.x)+abs(playerSpeed.y))*get_delta_time_for_a_tick());
				speedText->set_text(newStr);
			}
			mySurface->render();
			*/
		}
	private:
		mm::vec2 size;

		int numHealthTexts;
		std::unique_ptr<ui::surface> mySurface;
		ui::text* healthTexts[MAX_SUBSYSTEMS];
		double lastHealth[MAX_SUBSYSTEMS];
		int shakeHealthTillTick[MAX_SUBSYSTEMS]; //stores what tick shaking the object will stop, or -1 if the object is not shaking

		double healthAmmount[MAX_SUBSYSTEMS];
		ui::text* speedText;
		mm::vec2r playerSpeed;

		char newStr[256] = {0}; //working space

		bool is_linked = false;
};


#define additionalStars 1.82 
//const std::vector<ui::color> starColors = {{0xff,0xff,0xff,0xff},{0xfb,0xf3,0xf9,0xff},{0xba,0xd8,0xfc,0xff}};
const std::vector<ui::color> starColors = {{0xff,0xff,0xff,0x82},{0xee,0x9f,0xa7,0x82},{0xba,0xd8,0xfc,0x82}};
const std::vector<int> starSizes = {2,2,2,2,2,2,1,1,3};//{2,1,1,1}
const double lowDist = 2.f;//1/20.f;
const double highDist = 30.f;

auto get_star_vars(render::camera* camera)
{
	double max_screen_size = std::max(camera->screen.x,camera->screen.y)*additionalStars
#ifdef DEBUG
		;
#else
		*(1/max_zoom_out);
#endif
	double screen_area = camera->screen.x*camera->screen.y;
	int star_count = (max_screen_size*max_screen_size*additionalStars)/(7000);
	printf("%d stars\n",star_count);
	return std::make_tuple(max_screen_size,screen_area,star_count);
}

class UI_WorldView : public ui::element //Includes stars and rendered objects but not HUD objects
{
	public:
		UI_WorldView(ui::state uistate, render::camera* camera) :
			ui::element(uistate),
			game_render(uistate,*global_render_engine ,camera) //TODO MEGA
		{
			auto [max_screen_size, screen_area,star_count] = get_star_vars(camera);
			stars = std::make_unique<render::stars>(uistate, star_count, max_screen_size,max_screen_size,starSizes,starColors,camera,lowDist,highDist,render_ideal_tick_length);
			stars->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
			add_child(&*stars);
			add_child(game_render);
			set_focused_element(game_render);
			game_render.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

			this->camera = camera;

			this->last_size = positioning_rectangle.get_size();


		}
		/*void impl_render() override
		{
			if (last_size.x != positioning_rectangle.get_size().x && last_size.y != positioning_rectangle.get_size().y) //comparing with float is safe... I am checking for binary differences.
			{
				using namespace ui;
				mySurface->getelement(star_element)->~element(); //Yes, this is ok. I am reconstructing it right below VVV
				auto [max_screen_size, screen_area,star_count] = get_star_vars(camera);
				new (mySurface->getelement(star_element)) render::stars(uistate, star_count, max_screen_size,max_screen_size,starSizes,starColors,camera,lowDist,highDist);
				last_size = positioning_rectangle.get_size();
			}
		}*/
	private:
		ui::game_render game_render;
		std::unique_ptr<render::stars> stars;
		render::camera* camera;
		mm::vec2 last_size;
		int star_element;
};

class camera_controller : public ui::element
{
	public:
		camera_controller(ui::state uistate) : 
			ui::element(uistate)
		{
		}
		virtual void update() = 0;
};

enum class camera_control_button_bindings
{
	zoom,
	drag,
	toggle_chaser,
	SIZE
};
enum class camera_control_axes_bindings
{
	down,
	right,
	SIZE
};

const auto one_second_in_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::seconds(1)).count();
struct delta_time_keeper
{
	std::chrono::time_point<std::chrono::steady_clock> last_time;
	delta_time_keeper()
	{
		last_time = std::chrono::steady_clock::now();
	}
	auto get_delta_ns()
	{
		auto now = std::chrono::steady_clock::now();
		auto delta_time = now - last_time;
		last_time = now;

		auto delta_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time).count();
		return delta_ns;
	}
	float get_delta_seconds()
	{	
		return static_cast<float>(get_delta_ns()) / static_cast<float>(one_second_in_ns);
	}
};

class player_tracker
{
	mm::vec2r current_position;
	mm::vec2r last_position;

	mm::vec2r get_pos()
	{
		try
		{
			//TODO MEGA
			//return render::get_object_position(player_center_render_object_id);
			return {0,0,0};
		}
		catch (std::exception&)
		{
			printf("fail\n");
			return {0,0,0};
		}
	}
	public:
		player_tracker() :
			current_position(get_pos()),
			last_position(current_position)
		{
		}
		mm::vec2r get_position()
		{
			return current_position = get_pos();
		}
};



class chaser_camera_control : public camera_controller
{
	render::camera& camera;
	player_tracker tracker;
	double distance_threshold;

	bool should_move = false;
	double tolerance = 0.01;


	delta_time_keeper dt;
	public:
		bool factor_delta_time = true;
	
		bool control_rotation = true;
		bool control_position = true;

		bool locked = false;

		double camera_linear_speed;
		double camera_angular_speed;

		chaser_camera_control(ui::state uistate, render::camera& camera, double camera_linear_speed = 3.0*120.0, double camera_angular_speed = 0.03*120.0, double distance_threshold = 500.0) :
			camera_controller(uistate),
			camera(camera),
			camera_linear_speed(camera_linear_speed),
			camera_angular_speed(camera_angular_speed),
			distance_threshold(distance_threshold)
		{
		}
		chaser_camera_control(chaser_camera_control const & other) = delete; 
		void update()
		{
			auto delta_s = dt.get_delta_seconds();


			double delta_adjusted_linear_speed = camera_linear_speed * delta_s;
			double delta_adjusted_angular_speed = camera_angular_speed * delta_s;

			mm::vec2r player_position = tracker.get_position();

			
			if (locked)
			{
				if (control_position)
				{
					camera.pos.x = player_position.x;
					camera.pos.y = player_position.y;
				}
				if (control_rotation)
				{
					camera.pos.r = player_position.r;
				}
				return;
			}
			mm::vec2r current_position = camera.pos;
			mm::vec2r position_difference = player_position-current_position;
			if (mm::distance(current_position, player_position) > distance_threshold)
			{
				should_move = true;
			}
			if(control_position && should_move)
			{
				if (std::abs(position_difference.x) < tolerance && std::abs(position_difference.y) < tolerance)
				{
					should_move = false;
				}
				else
				{
					mm::vec2 direction_to_take = mm::unit_vector(position_difference);
					mm::vec2 ammount_to_move = direction_to_take * delta_adjusted_linear_speed;
					//prevent overshooting:
					double percent_left_to_move = position_difference.x/ammount_to_move.x;
					if (percent_left_to_move < 1.0)
					{
						ammount_to_move = ammount_to_move * percent_left_to_move;
					}
					camera.pos = camera.pos + ammount_to_move;
				}
			}
			//camera.pos = player_position;
			//basically, if the player is within a circle which has the radius half that of the smaller of the two screen sizes (e.x. height in 16:9), THEN we will rotate the screen.
			if (control_rotation && position_difference.magnitude() < std::min(camera.screen.x,camera.screen.y)/2.0)
			{
				position_difference.r = mm::canonicalize_rotation(position_difference.r);
				printf("dif %f casp %f\n",position_difference.r,delta_adjusted_angular_speed);
				double this_camera_angular_speed = delta_adjusted_angular_speed > position_difference.r? position_difference.r : delta_adjusted_angular_speed;
				if (position_difference.r > M_PI)
				{
					camera.pos.r -= this_camera_angular_speed;
					printf("1\n");
				}
				else
				{
					camera.pos.r += this_camera_angular_speed;
					printf("2\n");
				}
			}
		}
};
inline constexpr double smallest_angular_distance_between_angles(double angle0, double angle1)
{
	angle0 = mm::canonicalize_rotation(angle0);
	angle1 = mm::canonicalize_rotation(angle1);
	auto d0 = angle1 - angle0;
	if (d0 < 0)
	{
		d0 += 2*M_PI;
	}
	if (d0 > M_PI)
	{
		return d0 - 2*M_PI;
	}
	return d0;
}
const auto time_to_lock_chaser_cam = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::milliseconds(1000));
class mouse_camera_control : public camera_controller,
	public ui::has_event<input::computer::mouse_motion::event_data_t>,
	public ui::has_event<ui::binding_for_button<camera_control_button_bindings::zoom>>,
	public ui::has_event<ui::binding_for_button<camera_control_button_bindings::drag>>,
	public ui::has_event<ui::binding_for_button<camera_control_button_bindings::toggle_chaser>>,
	public ui::has_event<ui::on_unfocus>
{
	std::chrono::time_point<std::chrono::steady_clock> time_activated_chaser;

	delta_time_keeper dt;
	player_tracker tracker;
	public:
		mouse_camera_control(ui::state uistate, render::camera& camera, double scroll_sensitivity) : 
			camera_controller(uistate),
			camera(camera),
			scroll_sensitivity(scroll_sensitivity),
			chaser(uistate, camera)
		{
			chaser.control_rotation = false;
			chaser.control_position = false;
			//chaser.factor_delta_time = false;
		}	
		mouse_camera_control(mouse_camera_control const & other) = delete;
		void impl_dispatch_event(ui::binding_for_button<camera_control_button_bindings::zoom> button) override
		{
			has_zooming_button_down = button.is_down;
		}
		void impl_dispatch_event(ui::binding_for_button<camera_control_button_bindings::drag> button) override
		{
			has_dragging_button_down = button.is_down;
		}
		void impl_dispatch_event(ui::binding_for_button<camera_control_button_bindings::toggle_chaser> button) override
		{
			if (!button.is_down)
			{
				if (chaser_control)
				{
					chaser.control_rotation = false;
					chaser.control_position = false;
					chaser.locked = false;
				}
				else
				{
					chaser.control_rotation = true;
					chaser.control_position = true;
					time_activated_chaser = std::chrono::steady_clock::now();
				}
				chaser_control = !chaser_control;
			}
		}
		void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override
		{
			if(has_dragging_button_down)
			{
				mm::vec2 drag = mm::vec2{static_cast<double>(motion.delta_x),static_cast<double>(motion.delta_y)}/-camera.scale;
				drag = mm::rotation_ccw(drag, camera.pos.r);
				camera.pos = camera.pos + drag;
			}
			else if (has_zooming_button_down)
			{
				camera.scale = camera.scale*(1.0+(motion.delta_y/ -100.0));
				camera.scale = std::clamp(camera.scale, max_zoom_out,max_zoom_in);
			}

			//I don't know what to call `v`, but basically what we're doing here
			//is, instead of rotating the world at a fixed point at the center of the screen,
			//we're instead rotating it around the cursor.
			//Really, we then re-project the cursor position in this to get its exact location
			//as a float, then set the cursor position to be that float.
			mm::vec2 before_cursor_pos = worldinput::mouse::get_cursor_world(&camera);

			double scroll_amount = motion.delta_wheel_y*-scroll_sensitivity;

			auto halfscreenw = -(camera.screen.x/camera.scale)/2+(motion.x/camera.scale);
			auto halfscreenh = -(camera.screen.y/camera.scale)/2+(motion.y/camera.scale);

			auto v0 = mm::vec2{halfscreenw,halfscreenh};
			auto v = mm::vec2{halfscreenw,halfscreenh};

			v = mm::rotation_cw(v, scroll_amount);
			v -= v0;
			v = mm::rotation_ccw(v, camera.pos.r);

			camera.pos += v;
			
			camera.pos.r += scroll_amount;

			//Correct the float cursor pos (deviate from the integer value given by OS)
			mm::vec2 after_cursor_pos = worldinput::mouse::get_cursor_world(&camera);
			auto difference_cursor = after_cursor_pos-before_cursor_pos;
			input::computer::mouse_motion::current_cursor_x -= difference_cursor.x;
			input::computer::mouse_motion::current_cursor_y -= difference_cursor.y;
		}
		void impl_dispatch_event(ui::on_unfocus) override
		{
			has_dragging_button_down = has_zooming_button_down = false;
		}
		void update()
		{
			auto delta_s = dt.get_delta_seconds();

			if (chaser_control)
			{
				mm::vec2r player_position = tracker.get_position();
				mm::vec2r position_difference = player_position-camera.pos;

				auto now = std::chrono::steady_clock::now();
				auto delta_since_activated = std::chrono::duration_cast<std::chrono::nanoseconds>(now - time_activated_chaser);
				double percentage_time_passed = static_cast<double>(delta_since_activated.count()) / static_cast<double>(time_to_lock_chaser_cam.count());
				//Derived from spring equations, with unit-mass.

				if (percentage_time_passed < 1.1) // +0.1 to give it a little bit of extra time to factor in rounding errors
				{
					chaser.camera_linear_speed = calculate_speed(position_difference.magnitude(), percentage_time_passed);
					auto angle_dist = smallest_angular_distance_between_angles(player_position.r, camera.pos.r);
					chaser.camera_angular_speed = calculate_speed(angle_dist, percentage_time_passed);
				}
				else
				{
					chaser.locked = true;
				}

				chaser.update();
			}
		}
	private:
		double calculate_speed(double dist, double percentage_time_passed)
		{
			//I don't know why dividing halving the time passed seems to work but it does
			double inverse_k = std::pow((0.5/percentage_time_passed)/(2*M_PI), 2);

			return std::sqrt((dist*dist) / inverse_k);
		}
		bool has_dragging_button_down = false;
		bool has_zooming_button_down = false;
		render::camera& camera;
		double scroll_sensitivity;
		chaser_camera_control chaser;

		bool chaser_control = false;
};
class locked_camera_controller : public camera_controller
{
	render::camera& camera;

	bool has_zooming_button_down = false;
	public:
		locked_camera_controller(ui::state uistate, render::camera& camera) : //, mm::vec2r& player_position) :
			camera_controller(uistate),
			camera(camera)
		{
		}
		/*locked_camera_controller(locked_camera_controller const & other) : 
			camera(other.camera),
			player_position(other.player_position)//,
			//new_player_listener(new_player::interface,gen_callback(),{})
		{
		}*/
		void giveEvent(mm::vec2 parrentPosition, SDL_Event event)
		{
			//TODO make a zoom control class instead
			if ((event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP) && (event.button.button == SDL_BUTTON_RIGHT)) 
			{
				has_zooming_button_down = event.type == SDL_MOUSEBUTTONDOWN;	
			}
			else if (event.type == SDL_MOUSEMOTION )
			{
				if (has_zooming_button_down)
				{
					camera.scale = camera.scale*(1.0+(event.motion.yrel / -100.0));
					camera.scale = std::clamp(camera.scale, max_zoom_out,max_zoom_in);
					printf("%f\n",camera.scale);
				}
			}
		}
		void update()
		{
			printf("id %lu\n",player_center_render_object_id);
			try
			{
				//TODO MEGA
				//camera.pos = render::get_object_position(player_center_render_object_id);
			}
			catch (std::exception&)
			{
			}
		}
};
class UI_GameView : public ui::element, //Includes a UI_WorldView and HUD elements
	public ui::has_event<ui::binding_for_button<chat::keybinds::start_chat>>
{
	public:
		UI_GameView(ui::state uistate,render::camera* camera, camera_controller* in_camera_controller) :
			ui::element(uistate),
			world_view(uistate,camera),
			minimap(uistate, 0.02, {0x00,0x33,0x00,255},{0x33,0x99,0x00,0xFF}, camera),
			my_camera_controller(in_camera_controller)
		{
				add_child(world_view);
				set_focused_element(world_view);
				world_view.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
				add_child(*my_camera_controller);
				my_camera_controller->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
				add_child(minimap);
				
				minimap.positioning_rectangle.set_size({150,150});

				//TODO leak
				auto healthview = new UI_HealthView(uistate,mm::vec2{0,0},{300,200},camera);
				add_child(healthview);
				healthview->positioning_rectangle.set_origin_position_calculator([this](){return this->positioning_rectangle.get_position()+mm::vec2{this->positioning_rectangle.get_size().x-300,this->positioning_rectangle.get_size().y-200};});

				auto intxtstyle = ui::font_style_t{ui::color{0,0,0,255},"FreeSans",18};
				auto msgtxtstyle = ui::font_style_t{ui::color{255,255,255,255},"FreeSans",18};
				auto cst = chat::style_t{{{0,0,0,255},{0,0,0,255},{255,255,255,255},{255,255,255,255},intxtstyle},msgtxtstyle};
				my_chat = std::make_unique<chat::UI>(uistate, cst);
				add_child(*my_chat);
				my_chat->positioning_rectangle.set_size({500,500});
				my_chat->positioning_rectangle.set_origin_position_calculator([this](){return mm::vec2{0,positioning_rectangle.get_size().y};});
				my_chat->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::align_button,true);
		}
		void impl_render()
		{
			my_camera_controller->update();
		}
		void impl_dispatch_event(ui::binding_for_button<chat::keybinds::start_chat> button) override
		{
			if (button.is_down)
			{
				set_focused_element(*my_chat);
			}
		}
	private:
		UI_WorldView world_view;
		ui::minimap minimap;
		std::unique_ptr<camera_controller> my_camera_controller;
		std::unique_ptr<chat::UI> my_chat;
};
namespace call_in_physics
{
	void start_game()
	{
		//TODO MEGA
		/*
		int numobjs = 10;//00;
		int iter = sqrt(numobjs);

		for (int y=-(iter/2);y<=iter/2;y++)
		{
			for (int x=-(iter/2);x<=iter/2;x++)
			{
				double px = static_cast<double>(x*100);
				double py = static_cast<double>((y*200));
				if (!(mm::distance(0,0,px,py) <= 200))
				{
					new Wall(rstate,{px,py,0},{0,0,0,10,10},1);//Enemie(rstate,{static_cast<double>(i*100),200,0},0);
				}
			}
		}
		*/
	}
}
mm::vec2r player_position;
void displayGame(ui::window_manager* wm)
{
	camera.pos = {0,0,0};
	
	/* //TODO MEGA
	auto future = player_center_render_object_id_promise.get_future();
	future.wait();
	player_center_render_object_id = future.get();

	{
		auto _ = std::lock_guard(global_physics_engine->physics_engine_mutex);
		render::push_new_objects();
	}
	*/
	
	//TODO leaks eventually
	auto my_camera_controller = new mouse_camera_control(uistate,camera,0.1);
	//auto my_camera_controller = new locked_camera_controller(uistate,camera);
	//auto my_camera_controller = new chaser_camera_control(uistate,camera);
	
	//simply set the game view to the new background. only one background may exist
	wm->set_background(std::make_unique<UI_GameView>(uistate,&camera,my_camera_controller));
}
class UI_MenuButton : public ui::element
{
	public:
		UI_MenuButton(ui::state uistate, std::string text) :
			ui::element(uistate)
		{
			button = std::unique_ptr<ui::button>(new ui::button(uistate, menu_button_style, text, menu_button_font));
			positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
			positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);

			button->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);

			add_child(*button);
		}
		std::unique_ptr<ui::button> button;
};
std::string stringify_resolution(options::resolution_option_selected_t res)
{
	switch (res.index())
	{
		case 0:
			return std::to_string(static_cast<std::uint32_t>(std::get<mm::vec2>(res).x))+"x"+std::to_string(static_cast<std::uint32_t>(std::get<mm::vec2>(res).y));
		case 1:
			return "Guess";
		case 2:
			return "Multimonitor";
		default:
			return "unknown res_mode_t i: "+std::to_string(res.index());
	};
}
struct resolution_holder
{
	std::vector<options::resolution_option_selected_t> resolutions;
		
	template<bool check = true>
	void add_option_to_menu_list(auto& menu_list, options::resolution_option_selected_t& current_res, options::resolution_option_selected_t new_res)
	{
		auto descriptor = ui::regular_menu_item_descriptor
		{
			stringify_resolution(new_res),
			[&,new_res]()
			{
				current_res = new_res;
			}
		};
		bool do_not_add = false;
		for (auto res : resolutions)
		{
			if (res == new_res)
			{
				do_not_add = true;
				break;
			}
		}
		if (!check || !do_not_add)
			menu_list.add_regular_item(descriptor);
	}
	void add_options_to_menu_list(auto& menu_list, options::resolution_option_selected_t& current_res)
	{
		for(auto res : resolutions)
		{
			add_option_to_menu_list<false>(menu_list,current_res,res);
		}
	}
};

mm::vec2 default_options_menu_size = {900,600};

class UI_OptionsMenu : public ui::element
{
	public:
		UI_OptionsMenu(UI_OptionsMenu const &) = delete;
		UI_OptionsMenu(UI_OptionsMenu && ) = delete;
		UI_OptionsMenu* operator=(UI_OptionsMenu const &) = delete;
		UI_OptionsMenu* operator=(UI_OptionsMenu &&) = delete;
		UI_OptionsMenu(ui::state uistate, ui::window_manager* wm, mm::vec2 screenSize,ui::style style) :
			ui::element(uistate),
			window(uistate,"Options",(screenSize/2.0)-(default_options_menu_size)/2.0,default_options_menu_size,style),
			current_selected_resolution(global_options->resolution),
			current_fullscreen(global_options->fullscreen)
		{ //TODO leaks
			auto background = new phosphor_background{uistate,default_phosphor_background_style};	
			background->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(window.ui_surface.positioning_rectangle);

			window.ui_surface.addelement(background);


			auto padding = mm::vec2{0,8};
			double associated_padding = 4;
			resolution_changer = new ui::menu_list_button(uistate, my_menu_list_style, {185,50},stringify_resolution(current_selected_resolution),true,std::tuple{});
			my_resolution_holder.add_options_to_menu_list(*resolution_changer, current_selected_resolution);
			my_resolution_holder.add_option_to_menu_list(*resolution_changer, current_selected_resolution,rstate->get_resolution());

			printf("rs %f %f\n", resolution_changer->positioning_rectangle.get_size().x,resolution_changer->positioning_rectangle.get_size().y);
			//TODO: this suggests refactor at menu_list_button.
			//Furthermore, the thing should be able to easily have the size specified in terms of padding rather than real size.
			//	Or some combination, where the real size will be X unless it's bigger then padding Y.
			//		This should be done in positioning rectangle.
			auto fs_options = std::tuple
			{
				ui::regular_menu_item_descriptor{"Windowed", [this](){current_fullscreen = vkrender::fullscreen_mode_t::windowed;}},
				ui::regular_menu_item_descriptor{"Fullscreen Desktop", [this](){current_fullscreen = vkrender::fullscreen_mode_t::fullscreen_desktop;}},
				ui::regular_menu_item_descriptor{"Fullscreen", [this](){current_fullscreen = vkrender::fullscreen_mode_t::fullscreen;}}
			};

			//TODO doesn't properly distinguisb between desktop/regular, fix with refactor to UI system and deletion.
			fullscreen_changer = new ui::menu_list_button(uistate,my_menu_list_style, {350,50},current_fullscreen != vkrender::fullscreen_mode_t::windowed? "Fullscreen (Desktop?)" : "Windowed",true, fs_options);

			//TODO refresh rate changer
			//TOOD physics rate changer
			
			GE_Font nickfont = GE_Font_GetFont("FreeSans",35).value();
			auto text = new ui::text(uistate,"Nickname:", {{0xff,0x33,0x33,0xff},"FreeSans",35});
			
			text->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);

			nick_input = new ui::text_input(uistate,ui::color{0xff,0x0,0x00,255},ui::color{0x00,0x00,0x00,255},window_background_color_focused, window_background_color_unfocused,nickfont);
			
			nick_input->positioning_rectangle.set_size({300,50});
			nick_input->set_text(netplay::nickname);
			nick_input->set_cursor_position(nick_input->get_text().size());

			window.ui_surface.addelement(nick_input);
			window.ui_surface.addelement(text);
			window.ui_surface.addelement(fullscreen_changer);
			window.ui_surface.addelement(resolution_changer);

			resolution_changer->positioning_rectangle.set_origin_position_calculator([this,padding]()
			{
				return window.ui_surface.positioning_rectangle.get_position() + mm::vec2{padding.y, padding.y};
			});
			fullscreen_changer->positioning_rectangle.set_origin_position_calculator([this,padding]()
			{
				return resolution_changer->positioning_rectangle.get_bottom_left() + padding;
			});
			text->positioning_rectangle.set_origin_position_calculator([this,padding]()
			{
				return fullscreen_changer->positioning_rectangle.get_bottom_left() + padding;
			});
			nick_input->positioning_rectangle.set_origin_position_calculator([this,associated_padding,text]()
			{
				return text->positioning_rectangle.get_bottom_left() + mm::vec2{0,associated_padding};
			});

			resolution_changer->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(wm->painting_boundries_rectangle);
			fullscreen_changer->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(wm->painting_boundries_rectangle);
			
			window.ui_surface.positioning_rectangle.remove_child(fullscreen_changer->positioning_rectangle);
			window.ui_surface.positioning_rectangle.remove_child(text->positioning_rectangle);
			window.ui_surface.positioning_rectangle.remove_child(nick_input->positioning_rectangle);

			resolution_changer->positioning_rectangle.add_child_rectangle(fullscreen_changer->positioning_rectangle);
			fullscreen_changer->positioning_rectangle.add_child_rectangle(text->positioning_rectangle);
			text->positioning_rectangle.add_child_rectangle(nick_input->positioning_rectangle);

			mm::vec2 buttonsize = mm::vec2{100,40};
			apply_button = new UI_MenuButton(uistate,"Apply");
			window.ui_surface.addelement(apply_button);
			apply_button->positioning_rectangle.set_origin_position_calculator([this](){return window.ui_surface.positioning_rectangle.get_position()+window.ui_surface.positioning_rectangle.get_size();});
			apply_button->positioning_rectangle.set_size(buttonsize);
			apply_button->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::align_button,true);
			apply_button->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::align_right,true);
			apply_button->button->callback = [this,uistate]()
			{
				global_options->resolution = current_selected_resolution;
				global_options->fullscreen = current_fullscreen;
				global_options->nickname = nick_input->get_text();

				global_options->apply(rstate);
				try
				{
					options::save_options(*global_options);
				}
				catch(std::exception const & e)
				{
					printf("Failed to save options\n");
					printf("%s\n",e.what());
				}	
				return nullptr;
			};

			add_child_without_modfying_positioning_rectangles(&window);
			window.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(wm->painting_boundries_rectangle);
			set_focused_element(size_t(0)); //the window

			window.ui_title_bar.close_button.callback = [this, wm]()
			{
				auto _wm = wm; //before we `delete this` (which will also delete the memory of this callback lambda), put the wm pointer on the stack.
				wm->remove_window(this);
				delete this; //TODO
				return _wm; //we actually need to unwind all the way to the window manager; for we have modified its internal containers.
			};

			window.min_size.set_size(nick_input->positioning_rectangle.get_bottom_right() + apply_button->positioning_rectangle.get_size() - window.positioning_rectangle.get_position());
			printf("min %f %f\n", window.min_size.get_size().x,window.min_size.get_size().y);
		}
		bool is_in_bounds(mm::vec2 mouse) override
		{
			return window.is_in_bounds(mouse);
		}
	private:
		ui::menu_list_button* resolution_changer;
		ui::menu_list_button* fullscreen_changer;
		ui::text_input* nick_input;
		UI_MenuButton* apply_button;

		resolution_holder my_resolution_holder = {{options::special_res_option_t<vkrender::res_mode_t::guess>{}, mm::vec2{1024,768},mm::vec2{1080,720},mm::vec2{1280,720},mm::vec2{1280,800},mm::vec2{1360,768},mm::vec2{1366,768},mm::vec2{1600,900},mm::vec2{1920,1080},mm::vec2{2560,1440},mm::vec2{3840,2160},options::special_res_option_t<vkrender::res_mode_t::multimonitor>{}}};
		options::resolution_option_selected_t current_selected_resolution;
		vkrender::fullscreen_mode_t current_fullscreen;

		ui::window window;
};

class UI_MainMenu : public ui::element
{
	public:
		UI_MainMenu(ui::state uistate, ui::window_manager* wm, input::interfaces & input_if) :
			ui::element(uistate),
			wm(wm),
			world(uistate, &camera),
			title_text(uistate,"S P A C E G A M E",ui::color{0xff,0xff,0xff,0x00},GE_Font_GetFont("FreeMono",50).value()),//TTF_SetFontStyle(titleFont.font,TTF_STYLE_ITALIC);
			copyright_text(uistate,"©2017-2022 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version",ui::color{0xff,0xff,0xff,0xff},GE_Font_GetFont("FreeMono",17).value()),
			button_surface(uistate, ui::color{0x00,0x63,0x00,0xff})
		{
			world.positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(positioning_rectangle);
			add_child(world);
			add_child(title_text);
			add_child(copyright_text);
			add_child(button_surface);

			initial_drop_down_seconds = seconds_passed();

			auto calculateTitleYPos = [this](){return ((positioning_rectangle.get_size().y/2)-300);};
			title_text.positioning_rectangle.set_origin_position_calculator([this,calculateTitleYPos,seconds_passed]()
			{
				return mm::vec2
				{
					positioning_rectangle.get_size().x/2,
					/*((positioning_rectangle.get_size().y/2)-(250))*/calculateTitleYPos()+(std::sin((seconds_passed())))*20
				}+drop_down_menu_offset_calculator(drop_down_menu_animation_magnitude_menu,drop_down_menu_animation_seconds);//mm::vec2{0,drop_down_menu_animation_seconds*3};
			});
			title_text.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			title_text.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
			title_text.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);

			copyright_text.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
			copyright_text.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::align_right,true);
			copyright_text.positioning_rectangle.set_origin_position_calculator([this](){return mm::vec2{positioning_rectangle.get_size().x,positioning_rectangle.get_size().y-18};});


			double button_height = 80;
			double button_width = button_height*(58.0/19.0);
			
			double button_spacing = button_height+8.f;


			double currentButtonY = button_height;//175;

			auto background = new phosphor_background{uistate,default_phosphor_background_style};	
			button_surface.addelement(background);
			background->positioning_rectangle.set_origin_size_calculator([this](){return button_surface.positioning_rectangle.get_size()-mm::vec2{16,16};});
			background->positioning_rectangle.set_origin_position_calculator([this](){return button_surface.positioning_rectangle.get_position()+(button_surface.positioning_rectangle.get_size()/2);});
			background->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
			background->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);

			startGame = new UI_MenuButton(uistate,"Start Game");
			button_surface.addelement(startGame);
			startGame->positioning_rectangle.set_origin_position_calculator([this,currentButtonY,calculateTitleYPos](){return button_surface.positioning_rectangle.get_position()+mm::vec2{button_surface.positioning_rectangle.get_size().x/2,currentButtonY};});
			startGame->positioning_rectangle.set_size({button_width,button_height});

			//all callbacks killing the menu will return this. we can't just `return this` because of the
			//implicit dynamic cast that will happen, which is not ok because `this` has already been unallocated.
			startGame->button->callback = [this]()
			{
				auto callback_return_val = dynamic_cast<ui::element*>(this);
				auto wm = this->wm;
				close_dialogues();
				global_physics_engine->transient_callback([]()
				{
					netplay::stop_connections();
					netplay::begin_offline_mode();
					call_in_physics::start_game();
					printf("dun\n");
				});
				displayGame(wm); //this call invalidates all non-stack memory accesses
				return callback_return_val;
			};


			currentButtonY += button_spacing;
			quitGame = new UI_MenuButton(uistate,"Quit");
			button_surface.addelement(quitGame);
			quitGame->positioning_rectangle.set_origin_position_calculator([this,currentButtonY,calculateTitleYPos](){return button_surface.positioning_rectangle.get_position()+mm::vec2{button_surface.positioning_rectangle.get_size().x/2,currentButtonY};});
			quitGame->positioning_rectangle.set_size({button_width,button_height});

			quitGame->button->callback = [&input_if]()
			{
				input_if.quit.push_event({});
				input_if.quit.send_events();
				return nullptr;
			};


			currentButtonY += button_spacing;
			levelEditorButton = new UI_MenuButton(uistate,"Level Editor");
			button_surface.addelement(levelEditorButton);
			levelEditorButton->positioning_rectangle.set_origin_position_calculator([this,currentButtonY,calculateTitleYPos](){return button_surface.positioning_rectangle.get_position()+mm::vec2{button_surface.positioning_rectangle.get_size().x/2,currentButtonY};});
			levelEditorButton->positioning_rectangle.set_size({button_width,button_height});

			levelEditorButton->button->callback = [this]()
			{
				auto callback_return_val = dynamic_cast<ui::element*>(this);
				/* //TODO MEGA
				auto wm = this->wm;
				auto uistate = this->uistate;
				close_dialogues();

				auto st = ui::popup_ok_style_t{};
				st.window = style;
				st.buttonFont = ui::font_style_t{ui::color{0xff,0xff,0xff,0xff},"FreeSans",35};
				st.buttons = ui::button_style_t{ui::color{0x00,0x33,0x00,255},ui::color{0x55,0xee,0x22,0xff},ui::color{0x1e,0x88,0x1a,0xff}};
				st.buttonSize = mm::vec2{40,8};
				st.useSizeAsPadding = true;
				st.buttonSpacing = 15;
				st.buttonDistanceFromBottom = 10;

				level_editor::style newstyle = 
				{
					style,
					right_click_style,
					st
				};
				GE_CompletelyReset();

				auto my_camera_controller = new mouse_camera_control(uistate,camera,0.1);
				wm->set_background(std::make_unique<level_editor::editor_ui>(uistate,wm,&camera,my_camera_controller,newstyle));
				*/
				return callback_return_val;
			};


			currentButtonY += button_spacing;
			options = new UI_MenuButton(uistate,"Options");
			button_surface.addelement(options);
			options->positioning_rectangle.set_origin_position_calculator([this,currentButtonY,calculateTitleYPos](){return button_surface.positioning_rectangle.get_position()+mm::vec2{button_surface.positioning_rectangle.get_size().x/2,currentButtonY};});
			options->positioning_rectangle.set_size({button_width,button_height});

			options->button->callback = [this, wm,uistate]()
			{
				wm->add_window(new UI_OptionsMenu(uistate,wm,positioning_rectangle.get_size(),style));
				return nullptr;
			};

			currentButtonY += button_spacing;
			multi_join = new UI_MenuButton(uistate,"Join Game");
			button_surface.addelement(multi_join);
			multi_join->positioning_rectangle.set_origin_position_calculator([this,currentButtonY,calculateTitleYPos](){return button_surface.positioning_rectangle.get_position()+mm::vec2{button_surface.positioning_rectangle.get_size().x/2,currentButtonY};});
			multi_join->positioning_rectangle.set_size({button_width,button_height});

			multi_join->button->callback = [this, wm,uistate]()
			{
				/* // TODO MEGA
				if (multi_join_ok.has_value())
				{
					wm->set_focused_element(*multi_join_ok);
					return nullptr;
				}
				mm::vec2 size = {320,115};

				//create popup
				GE_Font rclickfont = GE_Font_GetFont("FreeSans",35).value();
				//TTF_Setfont_style_t(rclickfont.font,TTF_style_t_NORMAL | TTF_style_t_BOLD);

				mm::vec2 text_size = {300,50};
				auto multi_join_text_input = new ui::text_input(uistate,ui::color{0xff,0x0,0x00,255},ui::color{0x00,0x00,0x00,255},window_background_color_focused, window_background_color_unfocused,rclickfont);

				//create popup
				auto st = ui::popup_ok_style_t{};
				st.window = style;
				st.buttonFont = ui::font_style_t{ui::color{0xff,0xff,0xff,0xff},"FreeSans",20};
				st.buttons = ui::button_style_t{ui::color{0x1e,0x88,0x1a,0xff},ui::color{0x55,0x00,0x00,0xff},ui::color{0x99,0x00,0x00,0xff}};
				//st.buttons = ui::button_style_t{ui::color{0x00,0x33,0x00,255},ui::color{0x55,0xee,0x22,0xff},ui::color{0x1e,0x88,0x1a,0xff}};
				st.buttonSize = mm::vec2{10,2};
				st.useSizeAsPadding = true;
				st.buttonSpacing = 15;
				st.buttonDistanceFromBottom = 5;


			
				auto ok_c = [this,wm, multi_join_text_input]()
				{
					std::string hostname = multi_join_text_input->get_text();

					auto ptr = multi_join_ok.value(); 
					multi_join_ok = {}; //leave multi_join_ok in a valid state (i.e. don't leave let optional with has_value()->true hold nullptr). not necessary since main menu will be deleted but done anyway for correctness.

					displayGame(wm);
					{
						auto _ = std::lock_guard(physics_engine_mutex);
						netplay::join_server(SERVER_DEFAULT_PORT,hostname);	
					}

					delete ptr;
					wm->remove_window(ptr);
					return ptr;
				};
				auto cancel_c = [this,wm]()
				{
					auto ptr = multi_join_ok.value();
					multi_join_ok = {};
					delete ptr;
					wm->remove_window(ptr);
					return ptr;
				};

				multi_join_ok = {new ui::popup_ok(uistate, (positioning_rectangle.get_size()-size)/2, size, multi_join_text_input,"Join game...",  "Connect", "Cancel",st,ok_c,cancel_c)};

				auto& surface_rect = multi_join_ok.value()->ui_window.ui_surface.positioning_rectangle;
				mm::vec2 button_top_y = {0,surface_rect.get_bottom_left().y - multi_join_ok.value()->ok->positioning_rectangle.get_position().y};
				multi_join_text_input->positioning_rectangle.set_origin_position_calculator([this, button_top_y, &surface_rect]()
				{
					return surface_rect.get_position() + (surface_rect.get_size() - button_top_y) /2;
				});
				multi_join_text_input->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);
				multi_join_text_input->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);

				auto coeffecient_size = text_size.x / size.x;
				multi_join_text_input->positioning_rectangle.set_origin_size_calculator([this, coeffecient_size,text_size]()
				{
					return mm::vec2{coeffecient_size * multi_join_ok.value()->positioning_rectangle.get_size().x, text_size.y};
				});

				wm->add_window(multi_join_ok.value());
				*/

				return nullptr;
			};


			currentButtonY += button_spacing;
			multi_host = new UI_MenuButton(uistate,"Host Game");
			button_surface.addelement(multi_host);
			multi_host->positioning_rectangle.set_origin_position_calculator([this,currentButtonY,calculateTitleYPos](){return button_surface.positioning_rectangle.get_position()+mm::vec2{button_surface.positioning_rectangle.get_size().x/2,currentButtonY};});
			multi_host->positioning_rectangle.set_size({button_width,button_height});

			multi_host->button->callback = [this,uistate,wm]()
			{
				auto callback_return_val = dynamic_cast<ui::element*>(this);
				/* //TODO MEGA

				close_dialogues();
				mm::vec2 size = {400,150};


				auto st = ui::popup_ok_style_t{};
				st.window = style;
				st.buttonFont = ui::font_style_t{ui::color{0xff,0xff,0xff,0xff},"FreeSans",20};
				st.buttons = ui::button_style_t{ui::color{0x00,0x33,0x00,255},ui::color{0x55,0xee,0x22,0xff},ui::color{0x1e,0x88,0x1a,0xff}};
				st.buttonSize = mm::vec2{10,2};
				st.useSizeAsPadding = true;
				st.buttonSpacing = 15;
				st.buttonDistanceFromBottom = 5;

				ui::popup_message(uistate, wm, positioning_rectangle.get_size()/2, size,"Now Hosting Game...", "Make sure to port forward port " + std::to_string(SERVER_DEFAULT_PORT) + ".",{{0xff,0xff,0xff,0xff},style.standardFont.font} , st);

				physics_engine_mutex.lock();
				netplay::start_server(SERVER_DEFAULT_PORT);
				physics_engine_mutex.unlock();
				startTheGame();
				displayGame(wm); //this call invalidates all non-stack memory accesses
				*/
				return callback_return_val;
			};


			currentButtonY += button_height;
			button_surface.positioning_rectangle.set_origin_size_calculator([this,currentButtonY](){return mm::vec2{500,currentButtonY};});
			button_surface.positioning_rectangle.set_origin_position_calculator([&,this,calculateTitleYPos,button_height]()
			{
				return mm::vec2
				{
					positioning_rectangle.get_size().x/2,
					std::min( calculateTitleYPos()+175-(button_height/2.0) , (copyright_text.positioning_rectangle.get_position().y) - (button_surface.positioning_rectangle.get_size().y) )
				}+
				drop_down_menu_offset_calculator(drop_down_menu_animation_magnitude_menu,drop_down_menu_animation_seconds);
			});
			button_surface.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_x,true);

			drop_down_menu_animation_magnitude_menu.set_origin_size_calculator([this](){return mm::vec2{0,-(positioning_rectangle.get_size().y/2)-button_surface.positioning_rectangle.get_size().y};});
			positioning_rectangle.add_child_rectangle(drop_down_menu_animation_magnitude_menu);
			drop_down_menu_animation_magnitude_stars.set_origin_size_calculator([this](){return mm::vec2{0, -40000};});
			positioning_rectangle.add_child_rectangle(drop_down_menu_animation_magnitude_stars);

			camera.scale = default_camera_scale;
		}
		//cannot be moved or copied due to use of lambdas that capture `this`
		UI_MainMenu(UI_MainMenu && other) = delete;
		UI_MainMenu(UI_MainMenu const & other) = delete;
		UI_MainMenu& operator=(UI_MainMenu && other) = delete;
		UI_MainMenu& operator=(UI_MainMenu const & other) = delete;
		void close_dialogues()
		{
			//close any window dialog if it is open
			if (multi_join_ok.has_value())
			{
				delete *multi_join_ok;
			}
		}
		void impl_render() override
		{
			camera.pos = {seconds_passed()*(3.0*60.0),0,0};
			//titleText->positioning_rectangle.set_position({size.x/2,((size.y/2)-(250))+(std::sin((static_cast<double>(render_ticknum)/60)))*20});
			
			drop_down_menu_animation_seconds = seconds_passed() - initial_drop_down_seconds;
			camera.pos += drop_down_menu_offset_calculator(drop_down_menu_animation_magnitude_stars,drop_down_menu_animation_seconds);
			positioning_rectangle.recalculate();

		}
	private:
		ui::window_manager* wm;

		UI_WorldView world;
		ui::text title_text;
		ui::text copyright_text;
		ui::surface button_surface;

		UI_MenuButton* startGame;
		UI_MenuButton* quitGame;
		UI_MenuButton* levelEditorButton;
		UI_MenuButton* options;
		UI_MenuButton* multi_join;
		UI_MenuButton* multi_host;
		//ui::text* crazy_text;
		//mm::vec2 crazy_text_velocity;

		std::optional<ui::popup_ok*> multi_join_ok;


		float initial_drop_down_seconds;
		float drop_down_menu_animation_seconds = 0;
		const float drop_down_menu_animation_duration_till_done = 1.33;
		//uses its size for magnitude
		ui::positioning_rectangle_t drop_down_menu_animation_magnitude_menu;
		ui::positioning_rectangle_t drop_down_menu_animation_magnitude_stars;

		mm::vec2 drop_down_menu_offset_calculator(ui::positioning_rectangle_t & magnitude, float drop_down_menu_animation_seconds)//, float drop_down_menu_animation_duration_till_done)
		{
			return (drop_down_menu_animation_seconds > drop_down_menu_animation_duration_till_done)? mm::vec2{0,0} : magnitude.get_size()  - (magnitude.get_size() * (drop_down_menu_animation_seconds / drop_down_menu_animation_duration_till_done));
		}
};

auto shorthands = flag_interpreter::shorthands_t{
	{"s","server"},
	{"u","unittest"},
	{"j","join"},
	{"tr","tickrate"},
	{"fr","framerate"}
};

class spacegame : public game_base
{
	public:
		spacegame() : 
			game_base
		(
				"Spacegame"
#ifndef NDEBUG
				" DEVELOPER BUILD"
#endif
#if DEBUG_RENDERS_ENABLED()
				" WITH DEBUG RENDERING"
#endif
		)
		{
		}
		int game_main(int argc, char* argv[], engine_state & estate) override
		{
			auto input_if = input::interfaces{};

			{
				auto _ = std::lock_guard(input::axis::deadzones_mutex);
				//TODO: Are these indicies correct?
				input::axis::deadzones[0] = {0.125,-0.2};
				input::axis::deadzones[1] = {0.125,-0.125};
				input::axis::deadzones[3] = {0.125,-0.125};
				input::axis::deadzones[4] = {0.125,-0.125};
			}

			player_controls my_player_controls{input_if};

			my_player_controls.axes.hook_key_to_axis(SDL_SCANCODE_W, player_control_axes::forward, 0.0, -1.0);
			my_player_controls.axes.hook_key_to_axis(SDL_SCANCODE_S, player_control_axes::forward, 0.0, 1.0);
			my_player_controls.axes.hook_key_to_axis(SDL_SCANCODE_D, player_control_axes::perpendicular, 0.0, 1.0);
			my_player_controls.axes.hook_key_to_axis(SDL_SCANCODE_A, player_control_axes::perpendicular, 0.0, -1.0);
			my_player_controls.axes.hook_key_to_axis(SDL_SCANCODE_Q, player_control_axes::angular, 0.0, 1.0);
			my_player_controls.axes.hook_key_to_axis(SDL_SCANCODE_E, player_control_axes::angular, 0.0, -1.0);

			my_player_controls.axes.hook_joystick_to_axis(5, player_control_axes::forward,-1.0);
			my_player_controls.axes.hook_joystick_to_axis(2, player_control_axes::forward,1.0);

			my_player_controls.axes.hook_joystick_to_axis(3, player_control_axes::perpendicular,1.0);

			my_player_controls.axes.hook_joystick_to_axis(0, player_control_axes::angular,-1.0);

			my_player_controls.buttons.hook_key_to_button(SDL_SCANCODE_SPACE, player_control_buttons::shoot);
			my_player_controls.buttons.hook_key_to_button(SDL_SCANCODE_F, player_control_buttons::dampen);

			my_player_controls.buttons.hook_joystick_button_to_button(0, player_control_buttons::shoot); //a
			my_player_controls.buttons.hook_joystick_button_to_button(2, player_control_buttons::dampen); //x

			input::remappable_buttons_t<camera_control_button_bindings> camera_binds{input_if};

			camera_binds.hook_key_to_button(SDL_SCANCODE_LALT, camera_control_button_bindings::zoom);
			camera_binds.hook_key_to_button(SDL_SCANCODE_C, camera_control_button_bindings::toggle_chaser);
			camera_binds.hook_mouse_button_to_button(input::computer::mouse_button::button_t::middle, camera_control_button_bindings::zoom);
			camera_binds.hook_mouse_button_to_button(input::computer::mouse_button::button_t::left, camera_control_button_bindings::drag);
			camera_binds.hook_mouse_button_to_button(input::computer::mouse_button::button_t::x1, camera_control_button_bindings::toggle_chaser);

			auto camera_binds_processor = ui::event_processor<camera_control_button_bindings, input::remappable_buttons_t<camera_control_button_bindings>>{camera_binds};
			
			auto physen = sim::physen{};
			global_physics_engine = &physen;

			physen.physics_engine_mutex.lock();
			auto rengine = render::render_engine{estate,physen};
			global_render_engine = &rengine;
			physen.physics_engine_mutex.unlock();


			/* //TODO MEGA
			event::listener<new_player::interface_t> new_player_listener = event::listener(new_player::interface, [&player](auto in)
			{
				if (in.controlling_player_id == netplay::my_client_id)
				{
					//GE_LinkVectorToPhysicsObjectPosition(in.pointer_to_player,&player_position); //TODO: Some code for keeping this glued only to 1 player at a time (though it is probably not possible for 1 client to have multiple players)
					//TODO update to new sync system
					player=in.pointer_to_player;
				}
			}, {});
			*/

			//Interpret estate.flags
			
			bool is_headless = estate.is_headless;
			bool start_server = flag_interpreter::flag_present(estate.flags, "server"); 
			bool join_server = flag_interpreter::flag_present(estate.flags, "join"); 
			bool do_show_main_menu = !(start_server || join_server || is_headless);
			bool do_show_game = (start_server || join_server) && !(is_headless);


			if (start_server && !(estate.flags["server"].size() <= 1))
			{
				throw std::runtime_error("server flag must have 0 or 1 arguments");
			}
			if (join_server && !(estate.flags["join"].size() > 0 && estate.flags["join"].size() <= 2))
			{
				throw std::runtime_error("join flag must have at 1 or 2 arguments");
			}

			//initialize the engine
			global_physics_engine->physics_engine_mutex.lock();


			//TODO deduplicate
			const unsigned long long nanoseconds_in_second = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::seconds(1)).count();
			if (flag_interpreter::flag_present_with_number_args(estate.flags,"tickrate",1))
			{
				unsigned long long tickrate = std::stoull(estate.flags["tickrate"][0]);
				global_physics_engine->physics_ideal_tick_length = std::chrono::nanoseconds( nanoseconds_in_second/tickrate);
				printf("Running at %llu ticks per second / %lu nanoseconds per tick\n",tickrate, global_physics_engine->physics_ideal_tick_length.count());
				
				if(tickrate > 512)
				{
					printf("	Holy shit.\n");
				}
				else if (tickrate == 69)
				{
					printf("	Nice.\n");
				}
				
				if (global_physics_engine->physics_ideal_tick_length.count() == 0)
				{
					throw std::runtime_error("cannot set the physics engine to infinite ticks per second");
				}
			}
			else
			{
				//TODO get rid of static cast. Why no div by float?
				global_physics_engine->physics_ideal_tick_length = std::chrono::nanoseconds( nanoseconds_in_second/ static_cast<unsigned long long>(estate.rstate->data.refresh));
			}
			if (flag_interpreter::flag_present_with_number_args(estate.flags,"framerate",1))
			{
				unsigned long long tickrate = std::stoull(estate.flags["framerate"][0]);
				render_ideal_tick_length = std::chrono::nanoseconds( nanoseconds_in_second/tickrate);
				printf("Running at %llu frames per second / %lu nanoseconds per frame\n",tickrate, render_ideal_tick_length.count());
				
				if(tickrate > 512)
				{
					printf("	Holy shit.\n");
				}
				else if (tickrate == 69)
				{
					printf("	Nice.\n");
				}
				
				if (render_ideal_tick_length.count() == 0)
				{
					throw std::runtime_error("cannot set the physics engine to infinite ticks per second");
				}
			}
			else
			{
				//TODO get rid of static cast. Why no div by float?
				render_ideal_tick_length = std::chrono::nanoseconds( nanoseconds_in_second/ static_cast<unsigned long long>(estate.rstate->data.refresh));
			}
			std::optional<worldinput::mouse::mouse_event_creator> mouse_event_creator;
			ui::window_manager* wm = estate.wm;
			if (!is_headless)
			{
				rstate = &(*estate.rstate);
				uistate = estate.uistate;
				auto _ = std::lock_guard(uistate.globallock->lock);

				global_options = &estate.options;

				render::load_all_sprites_in_dir(*estate.rstate, filesystem::concat_directories(filesystem::app_root_directory,"spacegame/sprites"));


				//initialize some fonts we use
				GE_Font_LoadFromDir(filesystem::concat_directories(filesystem::app_root_directory,"spacegame/fonts"));
				init_config();
				tiny_sans = GE_Font_GetFont("FreeSans",15).value();
				big_sans = GE_Font_GetFont("FreeSans",72).value();
				title_sans = GE_Font_GetFont("FreeSans",18).value();
				TTF_SetFontStyle(big_sans.font,TTF_STYLE_ITALIC);
				TTF_SetFontStyle(title_sans.font,TTF_STYLE_NORMAL | TTF_STYLE_BOLD);

				
				camera.pos = {0,0,0};
				camera.scale = default_camera_scale;
				camera.screen.x = rstate->get_resolution().x;
				camera.screen.y = rstate->get_resolution().y;


#if DEBUG_RENDERS_ENABLED()
				debug::pass_uistate(uistate,&camera);
#endif

				mouse_event_creator = worldinput::mouse::mouse_event_creator(&camera,input_if);

				wm->set_background(std::make_unique<UI_MainMenu>(uistate,wm,input_if));
			}

			//let the rstate know approx what tick it is
			render_ticknum=0;
			//GE_GlueTarget* ticknumGlue = GE_addGlueSubject(&render_ticknum,&ticknum, GE_PULL_ON_PHYSICS_TICK,sizeof(int)); //TODO REPLACE THIS WITH NEW GLUE SYSTEM
			//TODO: Remove when glue system is removed
			

			auto ecs = spacegame_ecs(*global_physics_engine, *global_render_engine, estate);

			ecs.make_wall({0,0,0});

			printf("Done with initial setup. Unlocking physics engine.\n");
			global_physics_engine->physics_engine_mutex.unlock();

			//initialize our game-specific classes
			//InitClasses(rstate, my_player_controls); //TODO MEGA


			if (do_show_main_menu)
			{
				//show_main_menu();
			}
			else if (do_show_game)
			{
				//displayGame();
			}
			//displayGame();
			//GE_JoinServer(SERVER_DEFAULT_PORT,"127.0.0.1");	
			
			if (start_server)
			{
				auto _ = std::lock_guard(global_physics_engine->physics_engine_mutex);
				netplay::start_server(estate.flags["server"].size() == 1? std::stoul(estate.flags["server"][0]) : SERVER_DEFAULT_PORT);
			}
			else if (join_server)
			{
				auto _ = std::lock_guard(global_physics_engine->physics_engine_mutex);
				netplay::join_server(estate.flags["join"].size() == 2? std::stoul(estate.flags["join"][1]) : SERVER_DEFAULT_PORT, estate.flags["join"][0]);
			}

			auto button_listener = event::listener(input_if.scan_code,[&](auto in)
			{
				if (!in.is_down && !input::computer::text_input::is_inputting())
				{
					if (in.scan_code == SDL_SCANCODE_O)
					{
						auto _ = std::lock_guard(uistate.globallock->lock);
						wm->add_window(new UI_OptionsMenu(uistate,wm,wm->positioning_rectangle.get_size(),style));
					}
#if DEBUG_RENDERS_ENABLED()
					else if (in.scan_code == SDL_SCANCODE_F1)
					{
						bump_current_debug_state();
					}
#endif
					else if (in.scan_code == SDL_SCANCODE_ESCAPE)
					{
						auto _ = std::lock_guard(uistate.globallock->lock);
						player = NULL; //this is a symptom of a design mistake
						wm->set_background(std::make_unique<UI_MainMenu>(uistate,wm,input_if));
					}
				}
			},{});
			
			while (running)
			{
				auto _ = algorithm::framerate_guard(render_ideal_tick_length);
				input_if.dispatch_events();

				
				if (is_headless)
				{
					std::this_thread::sleep_for(std::chrono::seconds(1));
					//headless contexts do not need very fast input handling
				}
				else
				{
					

					estate.rstate->api.begin_frame();

#if DEBUG_RENDERS_ENABLED()

					auto debug_state = debug_state_array[current_debug_state];
					
					//????
					if (debug_state.render_world || debug_state.render_ui)
					{
						auto _ = std::lock_guard(uistate.globallock->lock);
						wm->render();
					}
					debug::this_thread_instance.done_drawing();
					if (debug_state.render_debug)
					{
						global_physics_engine->physics_debug_instance->render();
						debug::this_thread_instance.render();

						debug::render_sticky_objects();
					}
#else
					{
						auto _ = std::lock_guard(uistate.globallock->lock);
						camera_binds_processor.give_all_events_to(*wm);
						wm->render();
					}
#endif
					estate.rstate->api.render();
					render_ticknum++;
				}
				//new_player::interface.dispatch_events(); //TODO MEGA
				//
			}
			//ShutdownClasses(); //TODO MEGA
			printf("Exit game\n");
			return 0;
		}
} spacegame_instance;
game_base* game = &spacegame_instance;
