/*!
 * @file
 * @author Jackson McNeill
 *
 * A standard level editor
 *
 * Copyright 2017-2020 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 *
 * Originally designated as a core engine component, now a part of the default spacegame "game". Some legacy naming is still present.
 */
#pragma once



#include <functional>
#include <type_traits>
#include <cassert>
#include <memory>
#include <optional>

#include "UI/UI.hpp"
#include "UI/color.hpp"

#include "render/game_render.hpp"
#include "render/camera.hpp"
#include "simulation/physics.hpp"

class GE_Stars;
namespace level_editor
{

	struct level_editor_object_properties
	{
		unsigned int numResizableRectangles;
		bool resize_enabled;
	};

	/*!
	 * Classes which register with the editor must inherit from this interface.
	 *
	 * All static values must be implemented. Their presence in this interface is for human readability purposes only (as they are used by the class registration template)
	 */
	class level_editor_interface
	{
		public:
			/*!
			 * Returns one of the resizable rectangles, in absolute cordinates.
			 */
			virtual mm::rectr getRectangle(unsigned int) 
			{
				assert(false);
				return mm::rectr{};
			};

			/*!
			 * Sets one of the rectangles. Runs with the physics thread locked. Given in absolute cordinates.
			 */
			virtual void setRectangle(unsigned int, mm::rectr)
			{
				assert(false);
			};
			
			/*!
			 * This should be implemented if you set the "resizeEnabled" property is set
			 */
			virtual void setSize(mm::vec2 size)
			{
				assert(false);
			};

			/*!
			 * This should be implemented if you set the "resizeEnabled" property is set
			 */
			virtual mm::vec2 getSize()
			{
				assert(false);
				return {0,0};
			};

			/*!
			 * Contains a default implementation -- suitable for most cases
			 */
			virtual void setPosition(mm::vec2r position)
			{
				dynamic_cast<sim::object*>(this)->position() = position;
			}

			/*!
			 * Contains a default implementation -- suitable for most cases
			 */
			virtual mm::vec2r getPosition()
			{
				return dynamic_cast<sim::object*>(this)->position();
			}

			/*!
			 * Simply holds the human-readable name of this class
			 */
			const static std::string name;

			/*!
			 * The properties of this object
			 */
			constexpr const static level_editor_object_properties properties = {0,false,};
			
			virtual ~level_editor_interface(){};
	};


	/*!
	 * return a new physics object when given the position under the cursor
	 */
	using GE_NewObject_t = std::function<sim::object*(render::state*,mm::vec2r)>;


	class editor_ui;


	inline bool single_run_if_object_at(mm::vec2 position, std::function<void(sim::object* obj)> function)
	{
		/* TODO MEGA
		std::set<sim::object*> objects = GE_GetObjectsInRadius(position,500);
		for( sim::object* obj : objects)
		{
			if (GE_IsPointInPhysicsObject(position,obj))
			{
				function(obj);
				return true;
			}
		}
		return false;*/
	}



	struct highlight_box_style
	{
		double linethickness;
		double draggableBoxDiameter;
		double rotatableBoxDiameter;
		ui::color boxColor;
		ui::color draggableBoxColor;
		ui::color rotatableBoxColor;
	};
	enum internal_draggablebox_position
	{
		topLeft,
		topRight,
		bottomRight,
		bottomLeft,
		middleTop,
		middleRight,
		middleBottom,
		middleLeft,
		center,
		rotater,
		hollowBox
	};


	/*!
	 * Gives a resizable, moveable, rotatable box. Positioned by CENTER!
	 */
	class highlight_box : public ui::element,
		public ui::has_event<input::computer::mouse_button::event_data_t>,
		public ui::has_event<input::computer::mouse_motion::event_data_t>
	{
		public:
			using callback_t = std::function<void(highlight_box&)>;
			highlight_box(ui::state uistate, render::camera* camera, editor_ui* levelEditor, mm::vec2r hostPosition, mm::rectr rectangle,bool resize_enabled, callback_t callback, highlight_box_style style);
			void impl_render() override;
			ui::positioning_rectangle_t* positioning_rectangle_t;
			mm::rectr getRectangle(); //TODO

			bool is_in_bounds(mm::vec2 mouse) override;

			void impl_dispatch_event(input::computer::mouse_button::event_data_t button) override;
			void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override;


			mm::vec2 size;
			mm::vec2r position;
		private:
			callback_t callback;
			bool resize_enabled; 
			std::unique_ptr<GE_RectangleShape> draggableBoxes;
			std::unique_ptr<GE_RectangleShape> rotatableBoxes;
			std::unique_ptr<GE_HollowRectangleShape> box;

			mm::vec2r originPosition; //TODO

			render::camera* camera;
			editor_ui* levelEditor;

			highlight_box_style style;

			bool is_being_dragged = false;
			internal_draggablebox_position box_being_dragged;
			bool resize_rectangle_by_center = false;


			mm::vec2 initial_size;
			mm::vec2r initial_position;
			bool expand_by_center = false; //basically, if true, when the corners are dragged both sides will expand equally. probably set when SHIFT is held down.

			int check_if_being_dragged(mm::vec2 mouse);

			mm::vec2r get_draggable_box_position(mm::vec2r top_left_parent_position,internal_draggablebox_position box, bool add_camera);
			mm::vec2r get_top_left_position();

			void render_draggable_box(unsigned int box, mm::vec2r top_left_pos);

			
	};


	struct style
	{
		ui::style general;
		ui::menu_list_style right_click_menu;
		ui::popup_ok_style_t popups;
	};


	class physics_object_highlight_box_manager;

	struct popup_position_ok_style
	{
		ui::popup_ok_style_t popupStyle;

	};
	class popup_position_ok : public ui::element
	{
		public:
			popup_position_ok(ui::state uistate, std::string text, mm::vec2 size, mm::vec2 position, double inputSpacing, ui::popup_ok_style_t style,std::function<void(mm::vec2r)> ok_callback, std::function<void(void)> cancel_callback);
			~popup_position_ok();
			void setCurrentVector(mm::vec2r vector);
			void setVector(mm::vec2r vector);

			void render() override;

			ui::popup_ok popup;
		private:
			mm::vec2 size;
			mm::vec2 position;
			double inputSpacing;
			ui::popup_ok_style_t style;
			ui::text_input* x_in = NULL;
			ui::text_input* y_in = NULL;
			ui::text_input* r_in = NULL;
			bool gotResult = false;
			mm::vec2r currentVector = {0,0,0};

			void convertStringedResultAndDumpIntoCurrentVector();
			std::function<void(mm::vec2r)> ok_callback;
			void submit();
	};


	class editor_ui : public ui::element,
		public ui::has_event_and_can_delete_parent_from_it<input::computer::mouse_button::event_data_t>
	{
		public:
			editor_ui(ui::state uistate, ui::window_manager* wm, render::camera* camera, ui::element* camera_controller, style my_style);
			~editor_ui();
			void impl_render() override;
			ui::element* impl_dispatch_event_maybe_delete_parent(input::computer::mouse_button::event_data_t button) override;

			render::camera* camera;
		private:
			ui::window_manager* wm;
			style my_style;
			ui::game_render level_renderer;
			ui::element* camera_controller;


			sim::object* focused_object = nullptr;

			std::unique_ptr<ui::menu_list> std_right_click_menu;
			std::unique_ptr<ui::menu_list> object_right_click_menu;

			ui::element* currently_open_right_click_menu = nullptr;

			GE_Stars* stars;

			std::optional<std::unique_ptr<physics_object_highlight_box_manager>> highlight_boxManager;
			std::unique_ptr<highlight_box> resize_manager;

			void close_any_right_click_menu();
			void closeObjectManipulators();

			std::optional<std::unique_ptr<popup_position_ok>> popupManager;
			level_editor_interface* popupSubjectObject; 
	};





	void internal_RegisterClassWithLevelEditor(std::string name, GE_NewObject_t allocationFunction, unsigned long long classID,level_editor_object_properties properties);


	template <class type>
	void register_class_with_level_editor(unsigned long long classID)
	{
		static_assert(std::is_base_of<level_editor_interface,type>::value,"register_class_with_level_editor - Class must inherit from level_editor_interface");
		internal_RegisterClassWithLevelEditor(type::name, type::spawnFromLevelEditor,classID,type::properties);	

	}
}
*/
