#include "level_editor.hpp"

#include <mutex>
#include <set>
#include <map>
#include <algorithm>

//Local includes

#include "render/stars.h"
#include "simulation/serializeObject.h"
#include "filesystem/filesystem.hpp"


//Debug includes

#include "render/debug.hpp"

namespace level_editor
{
	using class_id_t = unsigned long long;

	std::vector<std::string> classNames;
	std::vector<GE_NewObject_t> classCreationFunctions;
	std::vector<level_editor_object_properties> classProperties;
	//map our internal number to the real one
	std::map<class_id_t,class_id_t> classRealTypeIDToLevelTypeID;


	std::unique_ptr<highlight_box> create_resize_highlight_box(ui::state uistate, render::camera* camera, editor_ui* levelEditor, sim::object* object, bool resize_enabled, highlight_box_style style)
	{
		//TODO MEGA
		//auto interf = dynamic_cast<level_editor_interface*>(object);
		level_editor_interface* interf;
		mm::vec2r pos = interf->getPosition();
		mm::vec2 size = resize_enabled? interf->getSize() : object->collision_info.bounding_box;

		auto callback = [interf,resize_enabled](highlight_box& self)
		{
			auto _ = std::lock_guard(physics_engine_mutex);
			if (resize_enabled)
			{
				interf->setSize(self.size);
			}
			interf->setPosition(self.position);
		};
		auto my_highlight_box = std::unique_ptr<highlight_box>(new highlight_box(uistate,camera,levelEditor,{0,0,0},mm::rectr{pos.x,pos.y,pos.r,size.x,size.y},resize_enabled,callback,style));

		my_highlight_box->painting_boundries_rectangle.set_size({-999999,-9999999});
		my_highlight_box->painting_boundries_rectangle.set_size({999999,9999999});
		my_highlight_box->painting_boundries_rectangle.set_position({0,0});

		return std::move(my_highlight_box);
	}
	/*
	void physics_object_resize_manager::giveEvent(mm::vec2 parrentPosition, SDL_Event event)
	{
		auto interf = dynamic_cast<level_editor_interface*>(object);
		my_highlight_box->giveEvent(parrentPosition, event);

		physics_engine_mutex.lock();
		mm::rectr rect = my_highlight_box->getRectangle();
		if (resizeEnabled)
		{
			interf->setSize(mm::vec2{rect.w,rect.h});
		}
		interf->setPosition(mm::vec2r{rect.x,rect.y,rect.r});
		physics_engine_mutex.unlock();
	}
	*/


	class physics_object_highlight_box_manager
	{
		public:
			physics_object_highlight_box_manager(ui::state uistate, render::camera* camera, editor_ui* levelEditor, sim::object* object, unsigned int numBoxes, highlight_box_style style);
			~physics_object_highlight_box_manager();

			void render();
		private:
			sim::object* object;
			unsigned int numBoxes;
			std::vector<std::unique_ptr<highlight_box>> highlight_boxes;
	};

	physics_object_highlight_box_manager::physics_object_highlight_box_manager(ui::state uistate, render::camera* camera, editor_ui* levelEditor, sim::object* object, unsigned int numBoxes, highlight_box_style style)
	{
		this->numBoxes = numBoxes;
		this->object = object;
		for (unsigned int i=0;i!=numBoxes;i++)
		{
			auto interf = dynamic_cast<level_editor_interface*>(object);;
			auto rectangle = interf->getRectangle(i); 

			auto callback = [](highlight_box& self)
			{

			};
			
			highlight_boxes.push_back(std::move(std::unique_ptr<highlight_box>(new highlight_box(uistate,camera,levelEditor,interf->getPosition(),rectangle,true,callback,style))));
		}
	}
	physics_object_highlight_box_manager::~physics_object_highlight_box_manager(){}

	/*
	void physics_object_highlight_box_manager::giveEvent(mm::vec2 parrentPosition, SDL_Event event)
	{
		auto interf = dynamic_cast<level_editor_interface*>(object);
		unsigned int i = 0;
		for (auto& highlight_box : highlight_boxes)
		{
			i++;
			highlight_box->giveEvent(parrentPosition, event);
			physics_engine_mutex.lock();
			interf->setRectangle(i,highlight_box->getRectangle());
			physics_engine_mutex.unlock();
		}

			
	}
	*/
	void physics_object_highlight_box_manager::render()
	{
		for (auto& highlight_box : highlight_boxes)
		{
			highlight_box->render();
		}
	}


	highlight_box::highlight_box(ui::state uistate, render::camera* camera, editor_ui* levelEditor, mm::vec2r hostPosition, mm::rectr rectangle,bool resize_enabled, callback_t callback, highlight_box_style style) :
		ui::element(uistate),
		callback(callback)
	{
		this->resize_enabled = resize_enabled; 

		this->originPosition = hostPosition;

		this->position = hostPosition;
		this->position.x += rectangle.x;
		this->position.y += rectangle.y;
		this->position.r += rectangle.r;

		this->size = {rectangle.w,rectangle.h};
		this->style = style;
		this->camera = camera;
		this->levelEditor = levelEditor;

		this->draggableBoxes = std::unique_ptr<GE_RectangleShape>(new GE_RectangleShape(uistate.rstate,style.draggableBoxColor));
		this->rotatableBoxes = std::unique_ptr<GE_RectangleShape>(new GE_RectangleShape(uistate.rstate,style.rotatableBoxColor));

		this->box = std::unique_ptr<GE_HollowRectangleShape>(new GE_HollowRectangleShape(uistate.rstate,style.boxColor,style.linethickness));
	}
	internal_draggablebox_position getOppositeCorner(internal_draggablebox_position corner)
	{
		switch(corner)
		{
			case topLeft:
				return bottomRight;
				break;
			case topRight:
				return bottomLeft;
				break;
			case bottomRight:
				return topLeft;
				break;
			case bottomLeft:
				return topRight;
				break;
			case middleLeft:
				return middleRight;
				break;
			case middleRight:
				return middleLeft;
				break;
			case middleTop:
				return middleBottom;
				break;
			case middleBottom:
				return middleTop;
				break;
			default:
				return topLeft;
				break;
		}
	}

	mm::vec2r highlight_box::get_draggable_box_position(mm::vec2r top_left_parent_position,internal_draggablebox_position box, bool add_camera)
	{
		mm::vec2r point;
		switch(box)
		{
			case topLeft: 
				point = {0,0,0};
				break;
			case topRight: 
				point = {size.x,0,0};
				break;
			case bottomRight: 
				point = {size.x,size.y,0};
				break;
			case bottomLeft: 
				point = {0,size.y,0};
				break;
			case middleTop: 
				point = {size.x/2,0,0};
				break;
			case middleRight: 
				point = {size.x,size.y/2,0};
				break;
			case middleBottom:
				point = {size.x/2,size.y,0};
				break;
			case middleLeft: 
				point = {0,size.y/2,0};
				break;
			case hollowBox:
				[[fallthrough]];
			case center:
				point = {size.x/2,size.y/2,0};
				break;
			case rotater:
				point = {size.x/2,-size.y/4,0};
				break;


		}
		point.r = position.r;
			
		if(box != hollowBox)
		{
			//we "shrink" when the camera zooms in (or rather, we are sized independantly of camera zoom and thus _appear_ to shrink)
			//so therefore we divide our centering operation by the camera scale to account for this. otherwise the math will be wrong and the dots 
			//will no longer be positioned correctly on any camera zoom other than 1.0
			point = point-(mm::vec2{style.draggableBoxDiameter/2,style.draggableBoxDiameter/2}/camera->scale);
		}
		mm::rotation_ccw(&point);
		point = point+mm::vec2{top_left_parent_position.x,top_left_parent_position.y};

		if (add_camera)
		{
			//point.x = point.x*(levelEditor->camera->scale);
			//point.y = point.y*(levelEditor->camera->scale);
			point = GE_Applyrender::cameraOffset(camera,point);
		}
		return point;
	}
	mm::vec2r highlight_box::get_top_left_position()
	{
		mm::vec2r pos = mm::vec2r{0,0,0};
		pos = pos+ position;


		mm::vec2r top_left_pos = pos;
		mm::vec2 rotatedsize = size;
		mm::rotation_ccw(&rotatedsize,pos.r);
		top_left_pos.x -= rotatedsize.x/2;
		top_left_pos.y -= rotatedsize.y/2;
		return top_left_pos;
	}
	void highlight_box::impl_render()
	{
		mm::vec2r top_left_pos = get_top_left_position();
		if (resize_enabled)
		{
			mm::vec2r boxpos = get_draggable_box_position(top_left_pos,hollowBox,true);
			box->render(boxpos,size*levelEditor->camera->scale);
			for (unsigned int i=0;i!=10;i++)
			{
				render_draggable_box(i,top_left_pos);
			}	
		}	
		else
		{
			render_draggable_box(center,top_left_pos);
			render_draggable_box(rotater,top_left_pos);
		}
	}
	void highlight_box::render_draggable_box(unsigned int box, mm::vec2r top_left_pos)
	{
		auto box_size = mm::vec2{style.draggableBoxDiameter,style.draggableBoxDiameter};
		draggableBoxes->render(get_draggable_box_position(top_left_pos,static_cast<internal_draggablebox_position>(box),true),box_size,mm::vec2{0,0});
	}
	/*!
	 * essentially, correct for negatives when dragging stuff.
	 *
	 * For example, if I where trying to see the vector change from the perspective of the right side of a box, moving left would be a positive delta rather than a negative
	 *
	 * (meaning, this function would make it positive)
	 */
	mm::vec2 translate_vector_to_draggable_box_perspective(internal_draggablebox_position box, mm::vec2 vector)
	{
		switch(box)
		{
			case middleTop:
			case middleLeft:
			case topLeft:
				vector.x *= -1;
				vector.y *= -1;
				break;
			case topRight:
				vector.y *= -1;
				break;
			case bottomLeft:
				vector.x *= -1;
				break;
			case bottomRight:
			default:
				break;
		}
		return vector;
	}
	mm::vec2 resize_anchoring_a_point(mm::vec2r anchor,internal_draggablebox_position moved_point,mm::vec2 center,mm::vec2 initial_size, mm::vec2 new_size)
	{
		mm::vec2 deltaSize = new_size-initial_size;

		mm::vec2 result = deltaSize/2.0;

		//adjust which points are anchored by adjusting how the position changes
		result = translate_vector_to_draggable_box_perspective(moved_point, result);
		
		mm::rotation_ccw(&result,anchor.r);

		return result;
	}

	int highlight_box::check_if_being_dragged(mm::vec2 mouse)
	{
		mm::vec2r top_left_pos = get_top_left_position();
		for(int i=(resize_enabled? 0 : 8);i!=10;i++)
		{
			mm::vec2r pos = get_draggable_box_position(top_left_pos,static_cast<internal_draggablebox_position>(i),true);
			if (mm::distance(mouse,pos) <= (style.draggableBoxDiameter*2)) //check if the cursor is in a radius. this makes it much easier to grab the rectangles
			{
				return i;
			}
		}
		return -1;
	}
	bool highlight_box::is_in_bounds(mm::vec2 mouse)
	{
		return check_if_being_dragged(mouse) != -1;
	}
	void highlight_box::impl_dispatch_event(input::computer::mouse_button::event_data_t button)
	{
		using input::computer::mouse_button::button_t;
		if (button.button == button_t::left)
		{
			if (button.is_down)
			{
				auto _box_dragged_maybe = check_if_being_dragged({static_cast<double>(button.x),static_cast<double>(button.y)});
				is_being_dragged = _box_dragged_maybe != -1;
				if (is_being_dragged)
				{
					box_being_dragged = static_cast<internal_draggablebox_position>(_box_dragged_maybe);

					initial_size = size;
					initial_position=position;
				}
			}
			else
			{
				is_being_dragged = false;
			}
		}
	}
	void highlight_box::impl_dispatch_event(input::computer::mouse_motion::event_data_t motion)
	{
		if (is_being_dragged)
		{
			mm::vec2r mouse = camera.negate_camera_offset({static_cast<double>(motion.x),static_cast<double>(motion.y),0.0});
			mm::vec2r top_left_pos = get_top_left_position();
			if (static_cast<int>(box_being_dragged) <= 7 && resize_enabled) //handle moveable corners
			{
				//figure out the new rectangle size by moving a point
				mm::vec2r start_rot = get_draggable_box_position(top_left_pos,getOppositeCorner(box_being_dragged),false);
				mm::vec2 start = static_cast<mm::vec>(start_rot);
				mm::vec2 end = static_cast<mm::vec>(mouse);
					
				//find rect size
				mm::vec2 size_after_resizing_based_on_cursor= GE_PointsToRectangle(start,end,position.r);

				//correct for negatives
				size_after_resizing_based_on_cursor = translate_vector_to_draggable_box_perspective(box_being_dragged, size_after_resizing_based_on_cursor);
				size_after_resizing_based_on_cursor.x = std::max(size_after_resizing_based_on_cursor.x,0.0);
				size_after_resizing_based_on_cursor.y = std::max(size_after_resizing_based_on_cursor.y,0.0);

				//if they drag a middle one, then they're only resizing in 1 direction
				switch(box_being_dragged)
				{
					case middleRight:
						[[fallthrough]];
					case middleLeft:
						size_after_resizing_based_on_cursor.y = size.y;
						
						break;
					case middleBottom:
						[[fallthrough]];
					case middleTop:
						size_after_resizing_based_on_cursor.x = size.x;
						break;
					default:
						break;
				}
				if (!resize_rectangle_by_center)
				{
					position = position+(resize_anchoring_a_point(start_rot,box_being_dragged,static_cast<mm::vec2>(position),size,size_after_resizing_based_on_cursor));
				}
				size = size_after_resizing_based_on_cursor;
			}
			else if(box_being_dragged == center)
			{
				position.x = mouse.x;
				position.y = mouse.y;
			}
			else if (box_being_dragged == rotater)
			{
				position.r = (atan2((mouse.x-position.x),(mouse.y-position.y)))-M_PI;
			}
			callback(*this);
		}

	}


	/*
	void highlight_box::giveEvent(mm::vec2 parrentPosition, SDL_Event event)
	{
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_LSHIFT || event.key.keysym.sym == SDLK_RSHIFT)
			{
				resize_rectangle_by_center = true;
			}
		}
		else if (event.type == SDL_KEYUP)
		{
			if (event.key.keysym.sym == SDLK_LSHIFT || event.key.keysym.sym == SDLK_RSHIFT)
			{
				resize_rectangle_by_center = false;
			}
		}

	}
*/
	mm::rectr highlight_box::getRectangle()
	{
		auto ammountMoved = position-originPosition;
		return mm::rectr{ammountMoved.x,ammountMoved.y,ammountMoved.r,size.x,size.y};
	}

	const mm::vec2 popupSize = {300,400};
	highlight_box_style style_highlight_box = {2,8,4,{0x00,0xff,0x00,0xff},{0x00,0xff,0x00,0xff},{0x00,0xff,0x00,0xff}};
	editor_ui::editor_ui(ui::state uistate, ui::window_manager* wm, render::camera* camera, ui::element* camera_controller, style my_style) : 
		ui::element(uistate),
		camera(camera),
		wm(wm),
		my_style(my_style),
		level_renderer(uistate, camera),
		camera_controller(camera_controller)
	{
		add_child(level_renderer);
		level_renderer.positioning_rectangle.set_origin_size_calculator([this](){return positioning_rectangle.get_size();});
		level_renderer.add_child(camera_controller);
		camera_controller->positioning_rectangle.set_origin_position_and_size_to_be_always_same_as(level_renderer.positioning_rectangle);

		#define additionalStars 3
		double maxScreenSize = std::max(camera->screen.x,camera->screen.y)*additionalStars;
		std::vector<ui::color> starColors = {{0xff,0xff,0xff,0xff},{0xfb,0xf3,0xf9,0xff},{0xba,0xd8,0xfc,0xff}};
		std::vector<int> starSizes = {2,2,2,2,2,2,1,1,3};

		stars =  new GE_Stars(uistate, 1000*additionalStars, maxScreenSize,maxScreenSize,{2,1,1,1},starColors,camera,3.f,20.f);



		auto null_cback = [](){};
		auto clicked_any_cback = [this](std::string)
		{
			close_any_right_click_menu();
		};

		auto std_right_click_contents = std::tuple
		{
			ui::submenu_menu_item_descriptor{"New Object>", std::tuple{},true},
			ui::divider_menu_item_descriptor{},
			ui::regular_menu_item_descriptor{"Save", null_cback}
		};

		std_right_click_menu = std::unique_ptr<ui::menu_list>{new ui::menu_list(uistate, my_style.right_click_menu, {185, 50}, true, std_right_click_contents)};
		std_right_click_menu->clicked_any_callback = clicked_any_cback;
		std_right_click_menu->get_sub_menus()[0]->clicked_any_callback = clicked_any_cback;
		auto object_right_click_contents = std::tuple
		{
			ui::regular_menu_item_descriptor{"Collision Boxes...",null_cback},
			ui::divider_menu_item_descriptor{},
			ui::regular_menu_item_descriptor{"Position...",null_cback}
		};
		size_t i = 0;
		for (std::string name : classNames)
		{
			auto callback = [this,camera,i,uistate]()
			{
				auto position = static_cast<mm::vec2r>(std_right_click_menu->positioning_rectangle.get_position());
				position = GE_Negaterender::cameraOffset(camera, position);
				physics_engine_mutex.lock();
				classCreationFunctions[i](uistate.rstate,position);
				physics_engine_mutex.unlock();
			};
			std_right_click_menu->get_sub_menus()[0]->add_regular_item(ui::regular_menu_item_descriptor{name, callback});
			i++;
		}
		object_right_click_menu = std::unique_ptr<ui::menu_list>(new ui::menu_list{uistate, my_style.right_click_menu, {185, 50}, true, object_right_click_contents});
		object_right_click_menu->clicked_any_callback = clicked_any_cback;

		std_right_click_menu->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);
		object_right_click_menu->painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_rectangle);

		

		GE_PhysicsEngine_TickingObjectsEnabled = false;
		GE_PhysicsEngine_CollisionsEnabled = false;

#if DEBUG_RENDERS_ENABLED()
		debug::pass_uistate(uistate,camera);
#endif
	}
	editor_ui::~editor_ui()
	{
		printf("Delete level editor\n");
		delete camera_controller;
		{
			auto _ = std::lock_guard(physics_engine_mutex);
			GE_PhysicsEngine_TickingObjectsEnabled = true;
			GE_PhysicsEngine_CollisionsEnabled = true;
		}
	}
	void editor_ui::impl_render()
	{
		if(highlight_boxManager.has_value())
		{
			(*highlight_boxManager)->render();
		}
	}
	ui::element* editor_ui::impl_dispatch_event_maybe_delete_parent(input::computer::mouse_button::event_data_t button)
	{
		using input::computer::mouse_button::button_t;
		bool just_opened_right_click = false;
		close_any_right_click_menu();
		closeObjectManipulators();
		if (button.button == button_t::right)
		{
			just_opened_right_click = true;
			currently_open_right_click_menu = &(*std_right_click_menu);
		}
		if (just_opened_right_click)
		{
			currently_open_right_click_menu->positioning_rectangle.set_position({static_cast<double>(button.x),static_cast<double>(button.y)});
			wm->add_window(currently_open_right_click_menu);
		}

		if (button.button == button_t::left)
		{
			auto position = static_cast<mm::vec2>(GE_Negaterender::cameraOffset(camera, mm::vec2r{button.x, button.y,0}));
			auto _ = std::lock_guard(physics_engine_mutex);
			single_run_if_object_at(position, [this](sim::object* obj)
			{
				printf("f obj\n");
				resize_manager = create_resize_highlight_box(uistate,camera,this,obj,classProperties[classRealTypeIDToLevelTypeID[obj->type]].resize_enabled,style_highlight_box);
				wm->add_window(&(*resize_manager));
			});
			//We want to unwind all the way to the window manager, so that we can avoid causing a re-focus that will wind up un-focusing our new highlight box.
			if (resize_manager)
			{
				return wm;
			}
		}
		return nullptr;
	}
	/*
	void editor_ui::giveEvent(mm::vec2 parrentPosition, SDL_Event event)
	{
		if(rightClickOpen == NORMAL)
		{
			rightClickMenu_normal->giveEvent(parrentPosition,event);
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				auto selectedID = rightClickMenu_normal->get_selected();
				if (selectedID.has_value())
				{
					std::string devName = normal.get_dev_name_of(selectedID.value());
					unsigned int iterator = 0;
					for (std::string name : classNames)
					{
						if (devName == std::to_string(iterator))
						{
							double mousex;
							double mousey;
							getRealWorldCursorPosition(&mousex,&mousey);
							physics_engine_mutex.lock();
							classCreationFunctions[iterator](rstate,mm::vec2r{mousex,mousey,camera->pos.r});
							physics_engine_mutex.unlock();
						}
						iterator++;
					}
					if (devName == "save")
					{	
						serialization::serialization_state state = serialization::serialization_state(9001);
						{
							printf("saving\n");
							physics_engine_mutex.lock();
							GE_SerializedTrackedObjects(state);
							filesystem::write_to_file("/tmp/REMOVE-tempsave2",state.buffer,state.bufferUsed);
							physics_engine_mutex.unlock();
						}
					}
					if (!normal.is_submenu(devName))
					{
						close_any_right_click_menu();
					}
				}
			}
		}
		else if (rightClickOpen == OBJECT)
		{
			rightClickMenu_object->giveEvent(parrentPosition,event);
			auto selectedID = rightClickMenu_object->get_selected();
			if (selectedID.has_value())
			{
				std::string devName = object.get_dev_name_of(selectedID.value());
				if (devName == "collisionBoxes")
				{
					closeObjectManipulators();
					highlight_boxManager = std::unique_ptr<physics_object_highlight_box_manager>(new physics_object_highlight_box_manager(rstate,camera,this,rightClickMenuSelectedObject,classProperties[classRealTypeIDToLevelTypeID[rightClickMenuSelectedObject->type]].numResizableRectangles,style_highlight_box));
				}
				else if (devName == "positionChange")
				{
					printf("posichangtmp\n");
					if(popupManager.has_value()) return	;
					auto ok_c = [this](mm::vec2r result)
					{
						closeObjectManipulators();
						printf("result %f %f %f\n",result.x,result.y,result.r);
						popupSubjectObject->setPosition(result);
						this->popupManager.reset();
					};
					auto cancel_c = [this]()
					{
						this->popupManager.reset();
					};
					popupManager = {std::make_unique<popup_position_ok>(rstate,"Input mm::vec2r", (size-popupSize)/2.0,popupSize,15,my_style.popups,ok_c,cancel_c)};
					popupManager.value()->setVector(rightClickMenuSelectedObject->position);
					popupSubjectObject = dynamic_cast<level_editor_interface*>(rightClickMenuSelectedObject);
					//ui::set_top_element(popupManager.value().get()); //TODO WM
				}
				if (!object.is_submenu(devName))
				{
					close_any_right_click_menu();
				}
			}
		}
		double x,y;
		int screenX,screenY;
		
		getRealWorldCursorPosition(&x,&y);
		SDL_GetMouseState(&screenX,&screenY);


		
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.scancode <= 323)
			{
				keysHeld[event.key.keysym.scancode] = true;
			}
		}
		else if (event.type == SDL_KEYUP)
		{
			if (event.key.keysym.scancode <= 323)
			{
				keysHeld[event.key.keysym.scancode] = false;
			}
		}
		//Mouse events handling
		else if (event.type == SDL_MOUSEBUTTONUP || event.type == SDL_MOUSEBUTTONDOWN)
		{
			
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				if (rightClickMenuJustOpened)
				{
					rightClickMenuJustOpened = false;
				}
				else
				{
					switch(rightClickOpen)
					{
						case NONE:
							break;
						case NORMAL:
							if (!rightClickMenu_normal->in_focus)
							{
								close_any_right_click_menu();
							}
							break;
						case OBJECT:
							if (!rightClickMenu_object->in_focus)
							{
								close_any_right_click_menu();
							}
					}
				}
			}
			if (rightClickOpen == NONE)
			{

				if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				{
					physics_engine_mutex.lock();
					single_run_if_object_at(mm::vec2{x,y},[this](sim::object* obj) 
					{
						closeObjectManipulators();
					});

					physics_engine_mutex.unlock();
				}
				else if (event.type == SDL_MOUSEBUTTONUP && hasFocusedObject)
				{
					hasFocusedObject = false;
				}
				if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_RIGHT)
				{
					//Update the position to be where the user right clicked to open the menu at
					updateMovingObjectPosition();
					physics_engine_mutex.lock();
					bool foundObject = single_run_if_object_at(mm::vec2{x,y},[this,screenX,screenY](sim::object* obj) 
					{
						rightClickOpen = OBJECT;
						rightClickMenu_object->positioning_rectangle.set_position({static_cast<double>(screenX),static_cast<double>(screenY)});
						rightClickMenuSelectedObject = obj;
						rightClickMenuJustOpened = true;
					});
					physics_engine_mutex.unlock();

					if (!foundObject)
					{
						rightClickOpen = NORMAL;
						rightClickMenu_normal->positioning_rectangle.set_position({static_cast<double>(screenX),static_cast<double>(screenY)});
						rightClickMenuJustOpened = true;
					}
				}
			}
		}
		if (rightClickOpen == NONE)
		{
			//highlight_box->giveEvent(parrentPosition,event);
			if(highlight_boxManager.has_value())
			{
				(*highlight_boxManager)->giveEvent(parrentPosition,event);
			}
			{
			}
			if (hasFocusedObject)
			{
				updateMovingObjectPosition();
			}
		}


	}
*/
	void editor_ui::close_any_right_click_menu()
	{
		if (currently_open_right_click_menu != nullptr)
		{
			wm->remove_window(currently_open_right_click_menu);
			currently_open_right_click_menu = nullptr;
		}
	}
	void editor_ui::closeObjectManipulators()
	{
		if(highlight_boxManager.has_value())
		{
			highlight_boxManager.reset();
		}
		if(resize_manager)
		{
			wm->remove_window(&(*resize_manager));
			resize_manager.reset();
		}
	}

	void internal_RegisterClassWithLevelEditor(std::string name, GE_NewObject_t allocationFunction, unsigned long long classID,level_editor_object_properties properties)
	{
		classRealTypeIDToLevelTypeID.insert(std::make_pair(classID,classNames.size()));	
		classNames.push_back(name);
		classCreationFunctions.push_back(allocationFunction);
		classProperties.push_back(properties);
	}



	/*
	class popup_position_ok
	{
		public:
			popup_position_ok(render::state* rstate, std::string text, mm::vec2 size, mm::vec2 position, double inputSpacing, ui::popup_ok_style_t style);
			void spawnPopup();
			~popup_position_ok();
			std::optional<mm::vec2r> result();
			bool isWindowAlive();

		private:
			render::state* rstate;
			std::string text;
			mm::vec2 size;
			mm::vec2 position;
			double inputSpacing;
			ui::popup_ok_style_t style;
			bool windowIsAlive = false;
			ui::PopupOK* popup;
			ui::text_input* x_in;
			ui::text_input* y_in;
			ui::text_input* r_in;
	};
	*/
	popup_position_ok::popup_position_ok(ui::state uistate, std::string text, mm::vec2 size, mm::vec2 position, double inputSpacing, ui::popup_ok_style_t style,std::function<void(mm::vec2r)> ok_callback, std::function<void(void)> cancel_callback) :
		ui::element(uistate),
		popup(uistate,position,size,new ui::surface(uistate,style.window.window_style.background),text,"Apply","Cancel",style,[this,ok_callback]()
		{
			submit();
			return nullptr;
		},[cancel_callback]()
		{
			cancel_callback();
			return nullptr;
		}),
		ok_callback(ok_callback)
	{

		this->size = size;
		this->position = position;
		this->inputSpacing = inputSpacing;
		this->style = style;
		auto* surface = &popup.ui_window.ui_surface;
		
		x_in = new ui::text_input(uistate, {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0x00,0x00,0x00,0xff},{0x00,0x00,0x00,0xff},style.buttonFont.font);
		surface->addelement(x_in);
		x_in->positioning_rectangle.set_origin_position_calculator([inputSpacing,this]()
		{
			//auto center_between_top_of_window_and_start_of_buttons_y =  ( popup. popup.ui_window.ui_surface.positioning_rectangle.get_position();
			return mm::vec2{inputSpacing , popup.area_between_top_of_surface_and_top_of_buttons.get_size().y/2}+popup.ui_window.ui_surface.positioning_rectangle.get_position();
		});
		x_in->positioning_rectangle.set_origin_size_calculator([this](){return mm::vec2{(popup.ui_window.ui_surface.positioning_rectangle.get_size().x-(this->inputSpacing*4))/3,0};});
		x_in->set_text(std::to_string(currentVector.x));

		//x_in in will serve as the primary position calculator and the rest will simply add their spacing.

		y_in = new ui::text_input(uistate, {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0x00,0x00,0x00,0xff},{0x00,0x00,0x00,0xff},style.buttonFont.font);
		surface->addelement(y_in);
		y_in->positioning_rectangle.set_origin_position_calculator([this](){return x_in->positioning_rectangle.get_origin_position()+mm::vec2{x_in->positioning_rectangle.get_size().x,0}+mm::vec{this->inputSpacing,0};});
		y_in->positioning_rectangle.set_origin_size_calculator([this](){return x_in->positioning_rectangle.get_origin_size();});
		y_in->set_text(std::to_string(currentVector.y));
		r_in = new ui::text_input(uistate, {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0x00,0x00,0x00,0xff},{0x00,0x00,0x00,0xff},style.buttonFont.font);
		surface->addelement(r_in);
		r_in->set_text(std::to_string(currentVector.r));
		r_in->positioning_rectangle.set_origin_position_calculator([this](){return y_in->positioning_rectangle.get_origin_position()+mm::vec2{x_in->positioning_rectangle.get_size().x,0}+mm::vec{this->inputSpacing,0};});
		r_in->positioning_rectangle.set_origin_size_calculator([this](){return x_in->positioning_rectangle.get_origin_size();});


		//surface->positioning_rectangle.set_origin_position_calculator([&](){return popup.ui_window.positioning_rectangle.get_position();});
		//popup.ui_window.positioning_rectangle.add_child_rectangle(&surface->positioning_rectangle);

		x_in->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::size_y_is_min_size,true);
		y_in->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::size_y_is_min_size,true);
		r_in->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::size_y_is_min_size,true);

		x_in->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);
		y_in->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);
		r_in->positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::center_y,true);
	}
	void popup_position_ok::submit()
	{
		convertStringedResultAndDumpIntoCurrentVector();
		ok_callback(currentVector);	
	}
	popup_position_ok::~popup_position_ok()
	{
		printf("rip\n");
	}
	void popup_position_ok::setCurrentVector(mm::vec2r vector)
	{
		currentVector = vector;	
	}
	void popup_position_ok::convertStringedResultAndDumpIntoCurrentVector()
	{
		currentVector = {
			std::stod(x_in->get_text()),
			std::stod(y_in->get_text()),
			std::stod(r_in->get_text())
		};
		gotResult = true;
	}
	void popup_position_ok::setVector(mm::vec2r vector)
	{
		currentVector = vector;
		x_in->set_text(std::to_string(currentVector.x));
		y_in->set_text(std::to_string(currentVector.y));
		r_in->set_text(std::to_string(currentVector.r));
		gotResult = false;
	}

	void popup_position_ok::render()
	{
		popup.render();
	}
	/*
	void popup_position_ok::giveEvent(mm::vec2 parrentPosition,SDL_Event event)
	{
		popup.giveEvent(parrentPosition,event);
		if (event.type == SDL_KEYUP)
		{
			if (event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_RETURN2 || event.key.keysym.sym == SDLK_KP_ENTER)
			{
				submit();
			}
		}
	}
	*/
}
