#!/bin/sh
echo "Release mode build"

mkdir log
mkdir build_release
cd build_release

rm CMakeCache.txt
cmake .. -DRELEASE=on
echo ------------------------------------------------------------------------------------------------------------
make -j$(../.CORES.sh)
echo ------------------------------------------------------------------------------------------------------------
cd ../module2d/src/shaders
./shadercompile.sh
cd ../../../
