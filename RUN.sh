#!/bin/sh
#we have to use POSIX shell sadly since fish may not be installed

#Not intended to be pointed to on a steam release version of the game

if [ ! -f build_release/Spacegame ]; then
	pkexec ./FRESHINSTALL.sh
fi

#if not submodules/git setup properly(then the first ./BUILD actually failed probably)
result=$(ls module2d | wc -l)
if [ $result -le 1 ]; then
	git submodule update --init --recursive
	./RUN.sh
	exit
fi


if [ ! -f build_release/Spacegame ]; then
	zenity --info --text="Building Game..." &
fi
./BUILD.sh
./build_release/Spacegame
