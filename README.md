# I'm on Ubuntu or Linux Mint and don't know what to do

Press CTRL+ALT+t

Now paste `git clone --recurse-submodules https://gitlab.com/AI221/spacegame && cd spacegame && ./RUN.sh`(to paste into the terminal, right click and click paste), and press enter. When prompted for your password, enter the password you use to login.

The game will pop up within a minute or two.


# Building & running

To build a release build and run the game, you can just type or double-click

`./RUN.sh`

It depends on

* SDL2
* SDL2_TTF
* SDL_Image
* glm (for vkrender)
* Vulkan

Additionally, the tools needed to build are:

* gcc >= 11.2.0 (**older versions will definitely fail**; clang is not supported because it doesn't work with C++23)
* CMake >= 2.6

**It is highly recommended to build on Ubuntu 22.04 or later**

Spacegame's Module2D engine inherently uses a lot of metaprogramming. Therefore, **building** spacegame requires roughly 1.5GB of RAM per thread.
So, my 32-core 5950x requires 64GB of total RAM to build on all cores.

Running is targetted at roughly a 2010 computer's worth of performance.

# Windows builds

Windows builds can be made by cross-compiling from a Linux host. Building on Windows itself is not supported and Cygwin probably will not work. You will need the [MinGW-w64](http://mingw-w64.org/) version of GCC, which should be provided by your system. Spacegame requires that your mingw be built with POSIX thread support.

Additionally, for Vulkan, you will need a system install of Wine.

First, you will need to install dependancies into your system. Luckily I have automated this and it can be done just by running

`cd module2d/
./_WININSTALLLIBS.sh`

This will install header files and .a files into /usr/x86_64-w64-mingw32/.
Additionally, it will also create a `build_windows/` folder and install all neccessary windows DLLs into it.

Then, to create the executable you can just run

`./WINCOMPL.sh`

The exe will be placed into the `build_windows/` folder.  
